
# Introduction #

This is the router lab the [DE-CIX Academy](http://www.de-cix.net/academy) uses for their BGP seminar "BGP for networks who peer".

It contains of a few docker images plus one subdirectory for each experiment the students use during the seminar.

Each experiment has to be configured to the number of students participating - each student gets its own virtual router accessible using a normal web browser (each one on a different port of the host running the docker containers).

Networks between the routers are built using docker-compose, both for IPv4 and IPv6.

# Installation #

This router lab has been tested and runs under:

- CentOS 7
- Ubuntu Linux 18.04 LTS
- Ubuntu Linux 22.04 LTS

It might run using other environments as well, but this is untested.

Required are the following packages:

- Docker Community Edition
  - For Ubuntu: <https://docs.docker.com/install/linux/docker-ce/ubuntu/>
  - For CentOS: <https://docs.docker.com/install/linux/docker-ce/centos/>
- Docker-Compose: <https://docs.docker.com/compose/install/>
- Exabgp: <https://github.com/Exa-Networks/exabgp> (or use a packaged version)

It is recommended to use the newest Docker packages from the docker website as packages included in your linux distribution might be outdated.

Two experiments (0 and 5) also require an external router connected to the internet.

Get started by cloning this repository into a directory on your linux host.

## Ansible ##

You can also use Ansible for installation (currently Ubuntu hosts only. Pull request for Centos welcome).
Have a user named 'ansible' on your target machine and make sure it can `sudo`.

- File `ubuntu-install-bgplab.yml` installs all packages.
- File `generic-setup-iptables.yml` sets up iptables and one rule in IPv6 nat table for Experiment-00.

## HTTPS ##

In the current version `ttyd` only runs via http. To offer the lab via https you need to setup nginx as reverse proxy.

- make server reachable on port 80  (can be removed later)
- get a certificate using `certbot --nginx certonly` (make sure /etc/nginx/sites-enabled only contains `default`)
- adjust the servername in script `tools/create-nginx-proxy-files`
- run the script in directory `/etc/nginx/sites-enabled/`
- (re-)start nginx

# Alternative Installation: Containerlab #

Recently porting the DE-CIX Academy lab towards [Containerlab](https://containerlab.dev) has begun. Simply clone the repository on your containerlab host and do a `make experiment.clab.yml` in the experiments diretory. This is yet experimental and might not work.

# Running it #

## Docker and Docker-Compose ##

As Docker and Docker-Compose sometimes behave strangely there is a certain risk that your server becomes non-accessible when executing docker-compose (docker-compose uses iptables to isolate the containers from each other. Sometimes this isolates the whole server). To minimize the risk you should:

- not start docker automatically after boot
- enter a command like `sleep 300; reboot` before every `docker-compose up` which you cancel when everything is fine (or have console access to your server where you can reboot it)
- if after reboot and restart of docker the server still becomes unaccesible, remove the file `/var/lib/docker/network/files/local-kv.db`

## Exabgp ##

You need a recent version of `exabgp` for some experiments. A config file `exabgp.conf` if usually auto-generated. For dynamic route-announcements via exabgp you need to create two fifos named `/run/exabgp.in` and `/run/exabgp.out`. For this the script `setup-fifos` in the tools directory can be used.

# Docker Images #

You can build the required docker images yourself or you can pull them (if availble) from [Docker Hub](https://hub.docker.com).

## Image *docker-frrouting* ##

This docker image is the base of most experiments.
It builds a docker image which runs [FRRouting](https://frrouting.org) with a web-based user interface based on [ttyd](https://tsl0922.github.io/ttyd/) in a docker container.

Files:

- `Dockerfile` can be used to build the required docker image
- `daemons` and `daemons.conf` are copied into the image and are used to start the daemons required for frrouting.
- `rc.local` is the startup file for the container. It changes some system variables required for IPv6 and also creates a loopback interface called *dummy0*
- `supervisord.conf` takes care that the required processes are started (using *supervisord*)

To create the required docker image you can simply look at the file `doit` - you might want to change the name of the container (currently *wtremmel/frr*) to something of your own.

## Image *docker-connect* ##

This docker image connects to a real router and opens twenty webserver interfaces to that router using [ttyd](https://tsl0922.github.io/ttyd/) .

Currently router *r01-academy.de-cix.net* is used, to change that to a router of your own, adjust the content of the file `supervisord.conf`, also you must add ssh keyfiles (public and private) so you can log in. These are not included in this package. `Dockerfile` references files `academy-keyfile` and `academy-keyfile.pub` - use *ssh-keygen* to create your own files and make sure that you can log on to your router using them.

## Image *docker-routinator* ##

Very simple image derived from [NLNetlabs](https://nlnetlabs.nl) [Routinator](https://nlnetlabs.nl/projects/rpki/routinator/) image: it simply adds some commands to start the validator. Needed for the experiment about RPKI.

# Experiments #

In general you build each experiment for the number of participants in your training. There is usually a perl script named `create-docker-compose` which takes in argument `-n` the number of participants. This script creates the following files:

- `config/` - in this directory a config-directory for each router is created and populated with startup files and example solutions. You should remove the directory and all subdirectories of `config/` once the training is complete.
- `exabgp.conf` - for the experiments using Exabgp the config file is created
- `ex-` files starting with ex- are also auto-generated and usually take care of announcing some prefixes via exabgp
- `stdout` - on output the *docker-compose* file is shown - so you might redirect this into file `docker-compose.yml` (by calling the script using *./create-docker-compose -n 10 > docker-compose.yml*)

Also in each directory scripts to upload example solutions are present - you can call them with parameter `-n xx` to upload the solution to all (number *xx*) routers or using `-N y`  to only router number *y*.

## Experiment 0: Getting connected ##

In this experiment a "real" router is connected via a proxy. This proxy then feeds five IPv4 and five IPv6 prefixes to the participants.

### Setup ###

- change into directory `cd experiment-00`
- remove old config using `rm -rf config`
- create the docker-compose file and config files using `./create-docker-compose -n *n* > docker-compose.yml`
- start the containers with `docker-compose up`
- in a different window configure the proxy with

## Experiment 1: iBGP ##

This experiment emulates a circular network (each router is connected to two neighboring routers).

### Setup ###

- change into the experiment directory `cd experiment-01`
- remove any remaining old configurations `rm -rf config`
- create the config for your training. If you have *n* participants, call the script using `./create-docker-compose -n *n* > docker-compose.yml`
- start the  containers using `docker-compose up`

### Working ###

- Students routers are available on ports 9001 and up
- Students can save their configurations using `write mem` - this is then available under the `config/rXX` directory
- To restart a router use `docker container restart rXX` - be aware all config is lost unless it was saved using `write mem`
- For a shell inside a container use `docker container exec -it rXX bash`

### Shutting down ###

Once the experiment is finished you need to shut down the containers.

- use `ctrl-c` to stop *docker-compose*
- remove the containers using `docker-compose down` in the experiments directory
