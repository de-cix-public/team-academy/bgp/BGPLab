#!/usr/bin/perl
#
# 
#    AS Numbers: 64500 upwards
#    Connection to AS196610


use Getopt::Std;


getopts('r:t:n:h');

if ($opt_h || !$opt_n) {
  print <<EOF;
Usage: $0 -n <number of instances> -t [docker|clab]
EOF
  exit;
}
$opt_t = "docker" unless $opt_t;

if ($opt_t eq "docker") {
  open(OUT,">docker-compose.yml");

  print OUT <<EOF;
version: '2.4'
services:
  validator:
    image: nlnetlabs/routinator:latest
    hostname: validator
    container_name: validator
    cap_add:
           - ALL
    ports:
      - "3323:3323"
      - "9099:8323"
    networks:
      admin:
      validatornet:
        ipv4_address: 10.254.1.254

  proxy:
    image: wtremmel/frr:latest
    hostname: proxy
    container_name: proxy
    sysctls:
                net.ipv6.conf.all.disable_ipv6: 0
                net.ipv6.conf.all.forwarding: 1
    cap_add:
            - ALL
    expose:
            - "179"
    volumes:
      - ./config/proxy:/etc/frr
    networks:
      admin:
      validatornet:
      proxynet:
        ipv4_address: 10.99.1.254
        ipv6_address: 2001:db8:500:99::254

EOF

for ($i = 1; $i <= $opt_n; $i++) {
  my $nodestr = sprintf "%02d",$i;

  # setup chain of interfaces
  #


  print OUT <<EOF;
  r$nodestr:      
    image: wtremmel/frr:latest
    hostname: r$nodestr
    container_name: r$nodestr
    sysctls:
                net.ipv6.conf.all.disable_ipv6: 0
                net.ipv6.conf.all.forwarding: 1
    cap_add:
            - ALL
    expose:
            - "179"
    ports:
      - "90$nodestr:80"
      - "20$nodestr:23"
    volumes:
      - ./config/r$nodestr:/etc/frr
    networks:
      admin:
      validatornet:
      proxynet:
        ipv4_address: 10.99.1.1$nodestr
        ipv6_address: 2001:db8:500:99::1$nodestr

EOF

  }
  print OUT <<EOF;
networks:
  admin:
  validatornet:
    ipam:
      config:
        - subnet: 10.254.1.0/24
  proxynet:
    enable_ipv6: true
    ipam:
      config:
        - subnet: 10.99.1.0/24
        - subnet: 2001:db8:500:99::/64
EOF


  close OUT;
} elsif ($opt_t eq "clab") {
  open(OUT,">experiment.clab.yml");
  print OUT <<EOF;
name: experiment05
prefix: ""

topology:
  nodes:
    validator:
      kind: linux
      image: nlnetlabs/routinator:latest
      ports:
        - 3323:3323
        - 9099:8323
      exec:
        - "ip address add dev eth1 10.254.1.254/24"


    proxy:
      kind: linux
      image: wtremmel/frr:latest
      binds:
        - ./config/proxy:/etc/frr
      exec:
        - "ip address add dev eth1 10.99.1.254/24"
        - "ip address add dev eth1 2001:db8:500:99::254/64"
        - "ip route add 91.214.253.0/24 nexthop via 172.20.20.1"
        - "ip route add 2a02:c50:db8::/48 nexthop via 3fff:172:20:20::1"
    
    proxynet:
      kind: bridge

    validatornet:
      kind: bridge

EOF
  for ($i = 1; $i <= $opt_n; $i++) {
    my $nodestr = sprintf "%02d",$i;
    print OUT <<EOF;
    r$nodestr:
      kind: linux
      image: wtremmel/frr:latest
      binds:
        - ./config/r$nodestr:/etc/frr
      ports:
        - 90$nodestr:80
        - 20$nodestr:23
      exec:
        - "ip address add dev eth1 10.99.1.1$nodestr/24"
        - "ip address add dev eth1 2001:db8:500:99::1$nodestr/64"
        - "ip address add dev eth2 10.254.1.1$nodestr/24"

EOF
  }
  my $pport = $opt_n+1;
  print OUT <<EOF;
  links:
    - endpoints: ["proxy:eth1","proxynet:prox$pport"]
    - endpoints: ["validator:eth1","validatornet:val$pport"]
EOF
  for ($i = 1; $i <= $opt_n; $i++) {
    my $nodestr = sprintf "%02d",$i;
    print OUT <<EOF;
    - endpoints: ["r$nodestr:eth1","proxynet:prox$i"]
    - endpoints: ["r$nodestr:eth2","validatornet:val$i"]
EOF
  }
  close OUT;
} else {
  die "choose either -t clab or -t docker";
}

for ($i = 1; $i <= $opt_n; $i++) {
  my $nodestr = sprintf "%02d",$i;

  # create and populate the volumes
  #
  system("mkdir -p config/r$nodestr");
  
  open(OUT,">config/r$nodestr/frr.conf");
print OUT <<EOF;
hostname r$nodestr
!
interface dummy0
  ip address 172.16.1.$i/32
!
interface eth1
!
interface eth2
!
interface eth3
!
route-map upstream-in permit 100
route-map upstream-out deny 100
!
router bgp 645$nodestr
  no bgp default ipv4-unicast
  neighbor upstream peer-group
  neighbor upstream-v6 peer-group
  address-family ipv4 unicast
    neighbor upstream activate
    neighbor upstream route-map upstream-in in
    neighbor upstream route-map upstream-out out
  exit-address-family
  address-family ipv6 unicast
    neighbor upstream-v6 activate
    neighbor upstream-v6 route-map upstream-in in
    neighbor upstream-v6 route-map upstream-out out
  exit-address-family
  neighbor 10.99.1.254 remote-as 196610
  neighbor 10.99.1.254 peer-group upstream
  neighbor 2001:db8:500:99::254 remote-as 196610
  neighbor 2001:db8:500:99::254 peer-group upstream-v6
!
EOF

  open(OUT,">config/r$nodestr/daemons");
print OUT <<EOF;
#
zebra=yes
bgpd=yes
ospfd=no
ospf6d=no
ripd=no
ripngd=no
isisd=no
ldpd=no
pimd=no
nhrpd=no
eigrpd=no
babeld=no
sharpd=no
pbrd=no
staticd=yes
bfdd=no
#
vtysh_enable=yes
zebra_options="  -s 90000000 --daemon -A 127.0.0.1"
bgpd_options="   --daemon -A 127.0.0.1 -M rpki -n"
ospfd_options="  --daemon -A 127.0.0.1"
ospf6d_options=" --daemon -A ::1"
ripd_options="   --daemon -A 127.0.0.1"
ripngd_options=" --daemon -A ::1"
isisd_options="  --daemon -A 127.0.0.1"
pimd_options="  --daemon -A 127.0.0.1"
ldpd_options="  --daemon -A 127.0.0.1"
nhrpd_options="  --daemon -A 127.0.0.1"
eigrpd_options="  --daemon -A 127.0.0.1"
babeld_options="  --daemon -A 127.0.0.1"
sharpd_options="  --daemon -A 127.0.0.1"
pbrd_options="  --daemon -A 127.0.0.1"
staticd_options="  --daemon -A 127.0.0.1"
bfdd_options="  --daemon -A 127.0.0.1"
fabricd_options="  --daemon -A 127.0.0.1"

# The list of daemons to watch is automatically generated by the init script.
watchfrr_enable=yes
# watchfrr_options=(-d -r /usr/sbin/servicebBfrrbBrestartbB%s -s /usr/sbin/servicebBfrrbBstartbB%s -k /usr/sbin/servicebBfrrbBstopbB%s -b bB)

# If valgrind_enable is 'yes' the frr daemons will be started via valgrind.
# The use case for doing so is tracking down memory leaks, etc in frr.
valgrind_enable=no
valgrind=/usr/bin/valgrind
EOF

  close OUT;

  open(OUT,">config/r$nodestr/solution-05-01-connect-rpki");
  print OUT <<EOF;

rpki stop
conf t
rpki
  rpki cache 10.254.1.254 3323 preference 1
end
rpki start
write mem

EOF

  close OUT;

  open(OUT,">config/r$nodestr/solution-05-02-upstream");
  print OUT <<EOF;
conf t

route-map upstream-in permit 100
route-map upstream-out deny 100

router bgp 645$nodestr
  neighbor upstream peer-group
  address-family ipv4
    neighbor upstream route-map upstream-in in
    neighbor upstream route-map upstream-out out
  exit-address-family
  neighbor 10.99.1.254 remote-as 196610
  neighbor 10.99.1.254 peer-group upstream

end
write mem
EOF
  close OUT;

  open(OUT,">config/r$nodestr/solution-05-03-invalids");
  print OUT <<EOF;
conf t

! route-map upstream-in deny 50
!   match rpki invalid
!
route-map rpki-invalid permit 100
  match rpki invalid
!
route-map rpki-valid permit 100
  match rpki valid
!
end
write mem
EOF
  close OUT;

  system("chmod 777 config/r$nodestr");
  system("chmod 666 config/r$nodestr/*");
}

system("mkdir -p config/proxy");
system("cp config/r01/daemons config/proxy");

open(OUT,">config/proxy/frr.conf");
print OUT <<EOF;
hostname proxy
!
!
ip route 91.214.253.0/24 10.99.1.1
ip route 192.168.99.0/24 dummy0
ipv6 route 2a02:c50:db8::/48 2001:db8:500:99::1
!
interface dummy0
 ip address 172.16.1.0/32
!
router bgp 196610
 neighbor internal peer-group
 neighbor internal remote-as 196610
 neighbor 91.214.253.254 peer-group internal
 neighbor 91.214.253.4 peer-group internal
 !
 neighbor internal-v6 peer-group
 neighbor internal-v6 remote-as 196610
 neighbor 2a02:c50:db8:1::1 peer-group internal-v6
 neighbor 2a02:c50:db8:3::2 peer-group internal-v6
 neighbor 2a02:c50:db8:3:a066:b1ff:fe64:7515 peer-group internal-v6
 !
 neighbor docker peer-group
 neighbor docker-v6 peer-group
EOF

for ($i = 1; $i <= $opt_n; $i++) {
  my $nodestr = sprintf "%02d",$i;
  print OUT <<EOF;
 !
 neighbor 10.99.1.1$nodestr remote-as 645$nodestr
 neighbor 10.99.1.1$nodestr peer-group docker
 neighbor 2001:db8:500:99::1$nodestr remote-as 645$nodestr
 neighbor 2001:db8:500:99::1$nodestr peer-group docker-v6
 !
EOF
}

print OUT <<EOF;
 !
 address-family ipv4 unicast
  network 192.168.99.0/24
  neighbor docker activate
  neighbor docker route-map deny-all in
  neighbor docker route-map permit-some out
  neighbor internal route-map permit-all in
  neighbor internal route-map deny-all out
  neighbor internal activate
  no neighbor internal-v6 activate
  no neighbor docker-v6 activate
 exit-address-family
 address-family ipv6 unicast
  no neighbor internal activate
  no neighbor docker activate
  neighbor internal-v6 activate
  neighbor internal-v6 route-map permit-all in
  neighbor internal-v6 route-map deny-all out
  neighbor docker-v6 activate
  neighbor docker-v6 route-map deny-all in
  neighbor docker-v6 route-map permit-some out
 exit-address-family
!
!
ip prefix-list ipv4-small-networks seq 15 permit 0.0.0.0/0 ge 21
!
ipv6 prefix-list ipv6-small-networks seq 15 permit ::/0 ge 32
!
route-map deny-all deny 100
!
route-map permit-all permit 100
!
route-map permit-some permit 25
  match rpki invalid
!
route-map permit-some deny 40
 match ipv6 address prefix-list ipv6-small-networks
!
route-map permit-some deny 50
  match ip address prefix-list ipv4-small-networks
!
route-map permit-some permit 100
!
!
EOF

system("chmod 777 config/proxy");
system("chmod 666 config/proxy/*");



