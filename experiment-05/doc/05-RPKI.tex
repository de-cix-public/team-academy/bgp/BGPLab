\documentclass[a4paper,notitlepage,oneside,]{article}
\usepackage[a4paper,margin=2.54cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[sfdefault,scaled=.85]{FiraSans}
\usepackage{newtxsf}
\usepackage{framed}
%
\usepackage{hyperref}
\usepackage[acronym,toc]{glossaries}
\usepackage{parskip}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{float}
\usepackage{enumitem}
\usepackage{fancyhdr}
\begin{document}
\frenchspacing
\pagestyle{fancyplain}
\renewcommand{\footrulewidth}{0.4pt}
\fancyhf{}
\fancyhf[HLE,HLO]{Experiment 5 - BGP and RPKI}
\fancyhf[HRE,HRO,FRE,FRO]{\thepage}
\fancyhf[FLE,FLO]{DE-CIX Academy}
\fancyhf[FCE,FCO]{BGP for networks who peer}
\title{Experiment 5 - BGP and RPKI}
\date{Version 0.3 - Students Copy}
\author{\href{mailto:academy@decix.net}{DE-CIX Academy}}
\maketitle

\section{Network Setup}
\begin{figure}[H]
  \centering
  \includegraphics[width=\linewidth,page=1]{img/05-rpki-drawings.pdf}
  \caption{Network Setup}
  \label{fig:networksetup}
\end{figure}
Figure \ref{fig:networksetup} shows the network topology for this experiment.
Upstream to AS196610 via a proxy router delivers real prefixes from the Internet. The caching validator runs in a separate container.

\section{Introduction}
\emph{RPKI} allows the holder of a resource (an IPv4/IPv6 prefix) to cryptographically prove that it is really the holder and allows via defining \emph{ROAs} how that prefix can be announced via BGP.

A \emph{ROA} is a triple containing the following values:
\begin{itemize}
  \item The prefix itself (network plus prefix length)
  \item An AS which is allowed to originate that prefix
  \item A maximum prefix length for which BGP announcements are allowed, this can be the same as the prefix length of the network (in this case the announcement of more specifics for this prefix is not allowed).
\end{itemize}

 To use RPKI and ROAs you need a host running a \emph{RPKI validator}. This validator fetches resource certificates and ROAs from RIRs, checks their signatures and is then available being contacted by routers using RPKI-RTR protocol.

 Routers simply receive a list of validated prefixes, their allowed originating AS number and a range of allowed networks masks. This can be used to check prefixes received via eBGP. Result of this check is one of three possible values:
 \begin{description}
   \item[Valid:] A ROA for this prefix exists, the originating AS matches and the announced prefix length is covered by the ROA.
   \item[Invalid:] A ROA for this prefix exists, but either it is for a different originating AS number or the prefix length is \emph{not} covered (too specific).
   \item[Unknown:] For this prefix a ROA does not exist. This result is also returned if no validator is reachable by the router. Most of the prefixes announced via eBGP fall into this category.
 \end{description}

\section{Starting RPKI}
RPKI can be stopped and started \emph{outside} the config mode. Use the following commands:
\begin{description}
  \item[rpki start] to start RPKI
  \item[rpki stop] to stop RPKI
\end{description}

\section{Config statements}
To configure RPKI you can use in \emph{config mode} (as always: Use ``conf term'' to enter):
\begin{description}
  \item[RPKI] changes the config context to \emph{rpki} mode. All following commands are to be entered in this mode. Use \emph{exit} to leave.
  \item[rpki cache [tcp|ssh] <ipv4 address> <portnumber> preference <number>] defines \emph{<ipv4 address>} as a validator. Your router will contact it via TCP on port \emph{<portnumber>}. Using the \emph{<preference>} parameter you can configure multiple validators and give them a preference.
  \item[rpki cache [tcp|ssh] <hostname> <portnumber> preference <number>] alternative version where the name of the validator is used instead of its IPv4 address.
\end{description}
This is the only command you need to configure a validator. For optional parameters please see the FRRouting documentation.

\subsection{Within route-maps}
There is only one match statement for RPKI.
\subsubsection{Match statements}
You can match against the three possible results of RPKI validation:
\begin{description}
  \item[match rpki \emph{valid}] returns true if the prefix has a covering ROA and originating AS and netmask evaluate ok.
  \item[match rpki \emph{invalid}] returns true if there is a ROA but either originating AS is wrong or announced netmask is not covered by the ROA.
  \item[match rpki \emph{notfound}] returns true if there is no ROA for the prefix.
\end{description}

\section{show commands}
The show statements are to list validators and the prefix tables received from them:
\begin{description}
  \item[show rpki cache-server] lists all the configured validators.
  \item[show rpki cache-connections] lists all the active connections to validators.
  \item[show rpki <prefix>] Looks if prefix (IPv4 or IPv6) is in the validated prefix table and shows its entry.
  \item[show rpki prefix-table] Lists the whole validated prefix table. Output can be long.
\end{description}

To show valid/invalid prefixes you need to define a route-map and then can use it in a show command:
\begin{verbatim}
conf term
route-map rpki-invalid
  match rpki invalid
end

show bgp ipv4 route-map rpki-invalid
show bgp ipv6 route-map rpki-invalid
\end{verbatim}

\section{Task: Reject prefixes with RPKI invalid}
\begin{itemize}
  \item Connect your router to a validator
  \item create a route-map which matches only invalid prefixes
  \item create a route-map which matches only valid prefixes
  \item have a look what (in)valid prefixes you receive from your upstream
  \item Update the \emph{upstream-in} route-map so prefixes with status \emph{invalid} are no longer accepted.
  \item check again for \emph{invalid} prefixes.
\end{itemize}

\appendix
\section{Initial Router Configuration}
Initially the router connects to AS196610 (DE-CIX Academy AS)
\begin{verbatim}
  hostname rxx
  !
  interface dummy0
   ip address 172.16.1.xx/32
  !
  router bgp 645xx
   neighbor upstream peer-group
   neighbor 10.99.1.254 remote-as 196610
   neighbor 10.99.1.254 peer-group upstream
   !
   address-family ipv4 unicast
    neighbor upstream route-map upstream-in in
    neighbor upstream route-map upstream-out out
   exit-address-family
  !
  route-map upstream-in permit 100
  !
  route-map upstream-out deny 100
  !
  end
\end{verbatim}

\section{Final Router Configuration}
Route-Maps \emph{rpki-invalid} and \emph{rpki-valid} are for show command only and not referenced (use \emph{show bgp ipv4 route-map invalid} for example).

\begin{verbatim}
  hostname rxx
  rpki
    rpki polling_period 3600
    rpki timeout 600
    rpki initial-synchronisation-timeout 30
    rpki cache tcp 10.254.1.254 3323 preference 1
    exit
  !
  interface dummy0
   ip address 172.16.1.xx/32
  !
  router bgp 645xx
   neighbor upstream peer-group
   neighbor 10.99.1.254 remote-as 196610
   neighbor 10.99.1.254 peer-group upstream
   !
   address-family ipv4 unicast
    neighbor upstream route-map upstream-in in
    neighbor upstream route-map upstream-out out
   exit-address-family
  !
  route-map rpki-invalid permit 100
   match rpki invalid
  !
  route-map rpki-valid permit 100
   match rpki valid
  !
  route-map upstream-in deny 50
   match rpki invalid
  !
  route-map upstream-in permit 100
  !
  route-map upstream-out deny 100
  !
  end
\end{verbatim}

\section{Slides}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Receive real prefixes via eBGP}
\LARGE
\begin{itemize}
\item For this we need real prefixes from the internet
\item Configuration needed will be preloaded - but for your reference here it is
\end{itemize}

\vspace{0.5cm}
\begin{framed}
\begin{verbatim}
conf t

route-map upstream-in permit 100
route-map upstream-out deny 100

router bgp 645xx
  neighbor upstream peer-group
  address-family ipv4
    neighbor upstream route-map upstream-in in
    neighbor upstream route-map upstream-out out
  exit-address-family
  neighbor 10.99.1.254 remote-as 196610
  neighbor 10.99.1.254 peer-group upstream
\end{verbatim}
\end{framed}

Have a look at your routing table:
\begin{itemize}
\item \textbf{show bgp ipv4 summary} - you should receive about 17000 prefixes
\item \textbf{show bgp ipv4} - beware, output is looong
\item \textbf{show bgp ipv4 statistics} - perhaps the most useful command for this experiment
\end{itemize}

\end{samepage}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Validator: Routinator}
\LARGE
\begin{itemize}
\item We are using \emph{Routinator} as validator
\item It also runs in a docker container
\item Just like your router, but on port \textbf{7099}
\end{itemize}

Try:
\begin{itemize}
  \item \url{https://bgplab.as196610.net:7099/}
  \item \url{https://bgplab.as196610.net:7099/status}
  \item \url{https://bgplab.as196610.net:7099/metrics}
  \item \url{https://bgplab.as196610.net:7099/csv}
  \item \url{https://bgplab.as196610.net:7099/json}
\end{itemize}

If you run into problems - check these URLs to see if the validator is running.

\end{samepage}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Connecting to the validator}
\LARGE
\begin{itemize}
\item a router connects to a validator using the
  \href{https://tools.ietf.org/html/rfc8210}{RPKI-RTR}
  protocol via TCP
\item we only have to configurate the validator IP address and portnumber
\item FRRouting uses a special rpki \emph{configuration context} for this
\end{itemize}

\vspace{0.5cm}
\begin{framed}
\begin{verbatim}
conf term

rpki
  rpki cache tcp 10.254.1.254 3323 preference 1
end
\end{verbatim}
\end{framed}

To see if RPKI is working use:
\begin{itemize}
\item \textbf{show rpki cache-connection} \\
  shows if connection to the validator is up
\item \textbf{show rpki cache-server} \\
  shows all configured validators
\item \textbf{show rpki prefix-table} (long output!) \\
  shows the complete table received from the validator
\item \textbf{show rpki prefix \emph{some prefix}} \\
  looks up \emph{some prefix} in the table of validated prefixes.
\end{itemize}

\vspace{1cm}
\Huge
You might have to start/stop rpki validation in FRRouting.
Use commands ``\emph{rpki stop}'' and ``\emph{rpki start}'' (\textbf{not} in config mode) for this


\end{samepage}

%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Route-Maps for RPKI}
\LARGE
\begin{itemize}
\item FRRouting does not filter by itself (other routers sometimes do)
\item So we need route-maps
\item We want one route-map for each RPKI state \emph{valid} and \emph{invalid}
\end{itemize}

\vspace{0.5cm}
\begin{framed}
\begin{verbatim}
conf t

route-map rpki-invalid permit 100
  match rpki invalid

route-map rpki-valid permit 100
  match rpki valid

\end{verbatim}
\end{framed}

We can now match against these route-maps:
\begin{itemize}
\item \textbf{show bgp ipv4 route-map rpki-valid} \\
  shows all the prefixes which are valid in terms of RPKI
\item \textbf{show bgp ipv4 route-map rpki-invalid} \\
  shows all prefixes which are invalid - you might check how using:
\item \textbf{show rpki prefix | include \emph{part of prefix}} \\
  shows only the part of the full rpki prefix table that matches \emph{part of prefix} as a text - quite useful if you want to figure out \emph{why} a prefix is invalid.
\end{itemize}

\end{samepage}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Dropping invalids}
\LARGE
\begin{itemize}
\item \emph{Invalid} prefixes (in terms of RPKI) are bad - so we do not want to accept them. Neither from upstream nor from peering.
\item with the match command we know, we can extend our \emph{upstream-in} route-map
\item check how many prefixes you receive:\\
\textbf{show bgp ipv4 statistics}
\item extend the \emph{upstream-in} route map with a \emph{deny} statement:
\end{itemize}

\vspace{0.5cm}
\begin{framed}
\begin{verbatim}
conf t

route-map upstream-in deny 50
  match rpki invalid
\end{verbatim}
\end{framed}

\begin{itemize}
\item \textbf{show bgp ipv4 statistics} \\
Enter this command \emph{before} and \emph{after} you add the deny-statement to your route-map.
\end{itemize}

\vspace{1cm}

\Huge
Congratulations!

\LARGE
You just made the Internet a little bit more safe!

\end{samepage}

\end{document}
