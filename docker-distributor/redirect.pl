#!/usr/bin/env perl

use Config::Simple;
use Data::Dumper;

$cfg = new Config::Simple('/etc/distributor/redirect.cfg');
$cfg = new Config::Simple('redirect.cfg') || die("No config file") unless $cfg;

$redirecturl = $cfg->param("redirecturl");
$myport = $cfg->param("myport");
$portbase = $cfg->param("portbase");
$maxsessions = $cfg->param("maxsessions");
$roundrobin = $cfg->param("roundrobin");
$counter = 1;
$logfile = $cfg->param("logfile");

if ($logfile) {
  open(F,">>$logfile");
  select F;
}

$| = 1; # Unbuffered output

$bodytemplate = <<EOF;
<head>
<meta http-equiv="refresh" content="2;url=\$redirecturl:\$thisport">
</head>
<body>
<h1>DE-CIX Academy Lab</h1>
<p>
You are on router R\$routerid
</p>
<p>
Please wait - if you are not automatically redirected, please click here:
<a href="\$redirecturl:\$thisport">\$redirecturl:\$thisport</a>
</p>
</body>
EOF

$fulltemplate = <<EOF;
<h1>DE-CIX Academy Lab: All seats are taken</h1>
<p>
Sorry, but all available seats in the lab are taken.
</p>
EOF


use Mojo::Server::Daemon;
my $daemon = Mojo::Server::Daemon->new(listen => ["http://*:$myport"]);
$daemon->unsubscribe('request')->on(request => sub {
  my ($daemon, $tx) = @_;
  $tx->res->code(200);
  $tx->res->headers->content_type("text/html");
  if ($counter > $maxsessions && $roundrobin) {
    $counter = 1;
  }
  if ($counter <= $maxsessions) {
	  $thisport = $portbase + $counter;
          $routerid = sprintf("%02d",$counter);
	  $tx->res->headers->append(Meta => "http-equiv=\"refresh\" content=\"1;url=$redirecturl:$thisport\"");
	  $body = $bodytemplate;
	  $body =~ s/(\$\w+)/$1/eeg;
	  print "Redirected " . $tx->remote_address . " to $redirecturl:$thisport\n";
  } else {
    $body = $fulltemplate;
  }
  $tx->res->body($body);
  $tx->resume;
  $counter++;
});
$daemon->run;


