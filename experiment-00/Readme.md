# Experiment 00 - Basic configuration and real prefixes
In this experiment a proxy router connects to a real router
connected to the default free zone and imports 5 prefixes for IPv4 and IPv6.

## Containerlab
You can also start the experiment using Containerlab - a clab file is also generated.