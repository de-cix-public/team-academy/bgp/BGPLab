\documentclass[a4paper,notitlepage,oneside,]{article}
\usepackage[a4paper,margin=2.54cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[sfdefault,scaled=.85]{FiraSans}
\usepackage{newtxsf}
\usepackage{framed}
%
\usepackage{hyperref}
\usepackage[acronym,toc]{glossaries}
\usepackage{parskip}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{float}
\usepackage{enumitem}
\usepackage{fancyhdr}
\begin{document}
\frenchspacing
\pagestyle{fancyplain}
\renewcommand{\footrulewidth}{0.4pt}
\fancyhf{}
\fancyhf[HLE,HLO]{Connecting to the Experiments}
\fancyhf[HRE,HRO,FRE,FRO]{\thepage}
\fancyhf[FLE,FLO]{DE-CIX Academy}
\fancyhf[FCE,FCO]{BGP for networks who peer}
\title{Connecting to the Experiments}
\author{\href{mailto:academy@decix.net}{DE-CIX Academy}}
\date{Version 3.1}
\maketitle
\section{Introduction}
  For our practical experiments during this training we are using virtualized machines running \href{https://www.docker.com}{Docker},
  and
  % \href{https://www.quagga.net}{Quagga} or
  \href{https://frrouting.org}{FRRouting}
  as routing software.

  Docker is a lightweight virtualization software, which allows running processes sandboxed in so-called containers.

  FRRouting is a fork and modern extension of well-known open source routing software Quagga, its command and configuration language is very much alike Cisco IOS. It contains a nearly full feature implementation of BGP (and also OSPF, IS-IS and others).

\section{Connecting}
\subsection{Connecting to \emph{your} Docker Container}
All docker containers run on the same host but on different ports. You can only connect with a browser. Try it out now:
\begin{itemize}
  \item Connect your laptop to wifi network
  \item You have received a card with the URL of \emph{your} router - just enter either the URL or the IP address in your browser. Do not forget the port number (":90xx").
\end{itemize}

\subsection{Working with FRRouting}
A shell to FRRouting should open. You can use your keyboard to enter commands (try \emph{show ip route}), change to config mode (\emph{configure}) and more. See below for some basic commands. You can use your computers cut and paste functionality as well.

A question-mark \emph{?} at any time shows you possible commands and command completions. For command completion you can also use the \emph{Tab} key of your computer (hit it twice to see multiple completion options).

To save your current configuration use the \emph{write mem} command.

FRRouting has two modes for interaction:
\begin{description}
  \item[Terminal mode] - this is the one you get when you connect. Here you can use the \emph{show} command to show you all kinds of information or you can save your configuration using \emph{write mem}.
  \item[Configuration mode] -  you enter configuration mode if you type \emph{configure} in terminal mode. Here you can do all kind of configuration changes. To leave configuration mode type ``\emph{end}''.
\end{description}
You see in which mode you in at the \emph{prompt} - in terminal mode the prompt is \texttt{routername\#}, in config mode the prompt is \texttt{routername(config)\#} .

\section{Some FRRouting Commands}
\subsection{Overview}
All commands can be entered shortened, so instead of ``configure'' you can also enter ``conf''. A question mark at any time shows you what you can enter, so if you enter ``conf?'' you see that you can complete this to ``configure''. A tabulator also completes the current command.

This lists only the very basic commands, you will learn about the rest during the training when you need them.

\subsection{Terminal mode - Basic commands}
\begin{description}
  \item [write terminal] lists current configuration. Useful for showing the config and the cut and pasting it into your local text editor.
  \item [show running-config] is the same
  \item [list] lists available commands
  \item [config terminal] changes into configuration mode
  \item [write mem] saves your current configuration to disk
  \item [terminal monitor] switch on receiving log messages on your terminal session
\end{description}

\subsection{Terminal mode - IP commands}
\begin{description}
  \item[show interface] shows you all interfaces, including IPv4 and IPv6 address details of all interfaces
  \item[show ip route] shows you the curent IPv4 routing table
  \item[show ipv6 route] does the same for IPv6
\end{description}

\subsection{Configuration mode - Basic commands}
All commands here are only valid when in \emph{config mode}. To enter config mode type ``configure'', to leave config mode type ``end''.
\begin{description}
  \item[end] leaves config mode
  \item[exit] leaves current config level. When on top, also leaves config mode.
  \item[no \ldots] negates a command or removes a line of configuration
  \item[interface \ldots] enters config mode of an interface. In interface config mode you can use commands like:
  \begin{description}
    \item[ip address \ldots] set ip address and netmask of interface
  \end{description}
  \item[router \ldots] enters config mode of a router daemon. This will be covered in later experiments. ``exit'' brings you back to top config mode.
  \item[! ] starts a comment. Comments are not saved on your router but come handy when preparing configurations in a text file.
\end{description}

\section{Slides}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Connecting}
\LARGE
\begin{itemize}
  \item You only need a web browser
  \item The URL of your router is on your router card
  \item All routers run on the same server
  \item They run on different \emph{ports} - if your card shows \emph{router id} \textbf{01} you connect to port \textbf{9001} and so on.
  \item Both \emph{http} and \emph{https} are available. \emph{http} is in the 9xxx port range, while \emph{https} is in the 7xxx port range.
  \item Connect to your router now
\end{itemize}

\subsection*{\Huge Entering commands}
\begin{itemize}
  \item Your router has two modes of operation
  \item \textbf{Terminal Mode} - this is the one you start with.
  \item The prompt in terminal mode is your router name followed by ``\#''
  \item Your router can \emph{autocomplete} commands - just use the \emph{tab} key (type twice for a list of possible completions).
  \item Or type ``?'' for a list of possible things you can enter
\end{itemize}

\vspace{1cm}

Lets try that now!

\begin{framed}
\begin{verbatim}
r01# sh?
\end{verbatim}
\end{framed}

\end{samepage}
%----------------------------------------------------------------
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Terminal Mode - more commands}
\Huge

Let's try the following commands and see the output:

\begin{framed}
\begin{verbatim}
show version

show running-config

terminal paginate

show interface

write mem

\end{verbatim}
\end{framed}

\Huge Important:
\begin{itemize}
  \item ``write mem'' saves your configuration
  \item please do this from time to time
\end{itemize}

\end{samepage}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Some commands have arguments}
\Huge

Let's have a look at "show interface":

\begin{framed}
\begin{verbatim}
show interface ?
\end{verbatim}
\end{framed}

\begin{itemize}
  \item You can add an interface name
  \item You can ask for a ``brief'' output
  \item Try it out!
\end{itemize}

\end{samepage}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Configuration mode}
\LARGE
\begin{itemize}
  \item The other mode is \emph{configuration mode}
  \item If you are in configuration mode, your prompt says:\\
   \texttt{r01(config)\#}
\end{itemize}
\vspace{1cm}

To get into and out of configuration mode:
\begin{framed}
\begin{verbatim}
r01# configure
r01(config)#
r01(config)#
r01(config)# end
r01#
\end{verbatim}
\end{framed}

What do we do in which mode?
\begin{itemize}
  \item in \emph{terminal mode} we mostly use the \texttt{show} command
  \item in \emph{configuration mode} well, we configure things
  \item to see what we have configured we can use:
  \begin{itemize}
    \item in terminal mode: \texttt{show running-config}
    \item in configuration mode: \texttt{do show running-config}
  \end{itemize}
  \item Remember: a ``?'' always tells you which commands are possible.
\end{itemize}

\end{samepage}
%----------------------------------------------------------------
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Let's configure something}
\LARGE
\begin{itemize}
  \item Something simple
  \item a \emph{static} route - in IPv4
  \item we put in a route for network \emph{10.120.1.0/24} pointing at interface \emph{dummy0}
  \item do a ``\texttt{show ip route}'' to see which routes already exist
  \item and then configure - see below. \\
  Here all commands are shown (on future slides we will skip the ``configure'' and ``end'' commands).
\end{itemize}

\vspace{1cm}

\begin{framed}
\begin{verbatim}
configure

! this is a comment
ip route 10.120.1.0/24 dummy0

end
\end{verbatim}
\end{framed}

Show commands:
\begin{itemize}
  \item show running-config
  \item show ip route
  \item show ip route 10.120.1.0
\end{itemize}
\end{samepage}
%----------------------------------------------------------------
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge BGP - some ``show'' commands}
\LARGE
\begin{itemize}
  \item We use BGP with \emph{IPv4} and \emph{IPv6}, so we must always tell the router which one we want
  \item the \emph{show} commands explained here will be needed over and over again
  \item Your router has a few prefixes from the internet - time to play!
\end{itemize}
\vspace{1cm}

Show commands to try out:
\begin{itemize}
  \item \texttt{show bgp ipv4 summary}\\
        \texttt{show bgp ipv6 summary}\\
        This shows you the state of your BGP \emph{sessions}
  \item \texttt{show bgp ipv4}\\
        \texttt{show bgp ipv6}\\
        Lists the complete BGP prefix table - currently very small, but on a full router this can be really long.
  \item \texttt{show bgp ipv4 a.b.c.d}\\
        \texttt{show bgp ipv6 aa:bb:cc:dd::}\\
        Get detailed information about a prefix. Try it with one of the prefixes you got as output from the previous command.
  \item \texttt{show bgp ipv4 | include <regex>}\\
        \texttt{show bgp ipv6 | include <regex>}\\
        Filters the output and only shows lines which match the regular expression \emph{<regex>}. 
        You can filter for all kinds of stuff. Keep in mind for later.
\end{itemize}
\end{samepage}
%----------------------------------------------------------------
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Debugging}
\LARGE
\begin{itemize}
  \item FRRouting has extensive debugging commands
  \item Debugging is configured in terminal mode
  \item You need to make the messages visible first
\end{itemize}
\vspace{1cm}

Show commands to try out:
\begin{itemize}
  \item \texttt{terminal monitor}\\
        makes logging and debugging messages visible
  \item \texttt{no terminal monitor}\\
        switches logging off again
  \item \texttt{debug bgp neighbor-events}\\
        starts debugging BGP neighbor related events
  \item \texttt{debug bgp ?}\\
        lists all possible BGP debugging subcommands.
  \item \texttt{no debug all}\\
        turn off all debugging
\end{itemize}
\end{samepage}
%----------------------------------------------------------------



\end{document}
