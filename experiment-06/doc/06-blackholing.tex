\documentclass[a4paper,notitlepage,oneside,]{article}
\usepackage[a4paper,margin=2.54cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[sfdefault,scaled=.85]{FiraSans}
\usepackage{newtxsf}
\usepackage{framed}
%
\usepackage{hyperref}
\usepackage[acronym,toc]{glossaries}
\usepackage{parskip}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{float}
\usepackage{enumitem}
\usepackage{fancyhdr}
\begin{document}
\frenchspacing
\pagestyle{fancyplain}
\renewcommand{\footrulewidth}{0.4pt}
\fancyhf{}
\fancyhf[HLE,HLO]{Experiment 6 - Blackholing via BGP}
\fancyhf[HRE,HRO,FRE,FRO]{\thepage}
\fancyhf[FLE,FLO]{DE-CIX Academy}
\fancyhf[FCE,FCO]{BGP for networks who peer}
\title{Experiment 6 - Blackholing via BGP}
\date{Version 1.0}
\author{\href{mailto:academy@decix.net}{DE-CIX Academy}}
\maketitle

\section{Network Setup}
\begin{figure}[H]
  \centering
  \includegraphics[width=\linewidth,page=1]{img/06-blackholing-drawings.pdf}
  \caption{Network Setup}
  \label{fig:networksetup}
\end{figure}
Figure \ref{fig:networksetup} shows the network topology for this experiment.
All students' routers connect to one upstream AS65550 (also running FRRouting in a Docker container)

\section{Introduction}
For this experiment we assume that one of our IPv4 addresses is being attacked and needs to be blackholed. The address under attack is \emph{172.16.x.2} (of course there is no real attack - the address is simply pinged from the upstream provider).

\section{Task: Request blackholing for one IP address}
\begin{itemize}
  \item Announce a /32 prefix of \emph{172.16.x.2} to your upstream provider via BGP.
  \item Have a BGP community of \emph{blackhole} (65535:666) attached to it.
  \item Build a scalable solution, so you need only one configuration statement to initiate blackholing.
\end{itemize}

\section{Config statements}
\subsection{Static routes}
\begin{description}
  \item[ip route \emph{a.b.c.d/32} blackhole tag \emph{nnnn}] adds a \emph{tagged} static route for prefix \emph{a.b.c.d/32}. Using \emph{blackhole} as gateway means that FRRouting drops all packets towards this prefix. The \emph{tag} can be used for matching in route-maps.
  \item[ipv6 route \emph{X:X::X:X/128} blackhole tag \emph{nnnn}] does the same for IPv6.
\end{description}

\subsection{Route maps and related}
\subsubsection{Prefix lists}
\begin{description}
  \item[ip prefix-list \emph{name} permit \emph{a.b.c.d/n}] machtes exactly for the prefix like it is stated.
  \item[ip prefix-list \emph{name} permit \emph{a.b.c.d/n} le \emph{m}] matches the prefix and also all \emph{more specifics} up to and including a prefix length of \emph{m}.
  \item[ipv6 prefix-list \emph{name} permit \emph{X:X::X:X/N}] machtes exactly for the IPv6 prefix like it is stated.
  \item[ipv6 prefix-list \emph{name} permit \emph{X:X::X:X/N} le \emph{M}] matches the prefix and also all \emph{more specifics} up to and including a prefix length of \emph{M}.
\end{description}

\subsubsection{Community lists}
Community lists match against communities attached on BGP prefixes. For this experiment we only need \emph{standard} community lists (\emph{expanded} community lists can match against \emph{regular expressions}):
\begin{description}
  \item[bgp community-list standard \emph{name} permit \emph{blackhole}] matches against all BGP prefixes which have the \emph{blackhole} community attached to it.
\end{description}

\subsubsection{Match statements}
\begin{description}
  \item[match tag \emph{nnnn}] matches against prefixes who are \emph{tagged} with a specific number.
  \item[match ip address prefix-list \emph{name}]
  \item[match ipv6 address prefix-list \emph{name}] matches against the named prefix list.
\end{description}

\subsubsection{Set statements}
We need to add a blackhole community to a prefix:
\begin{description}
  \item[set community \emph{blackhole} additive] adds (the \emph{additive} keyword, without it we would delete all existing communities) a well-known community named \emph{blackhole} to a BGP prefix.
\end{description}

\subsection{BGP}
\subsubsection{Redistributing prefixes}
We already know the ``network'' statement for redistributing, but you can also use the ``redistribute'' config command. Both commands are configured in the ``address-family'' context of BGP:
\begin{description}
  \item[redistribute \emph{static} route-map \emph{<route-map-name>}] redistribute static routes after evaluating them with a route-map. Instead of ``static'' you can use any routing protocol.
\end{description}

\subsubsection{Global parameters}
In the configuration of the ``upstream'' router you will find a command you have not seen yet:
\begin{description}
  \item[bgp disable-ebgp-connected-route-check] allows prefixes into the BGP table which are considered to be \emph{unreachable}. As blackholed prefixes use a next-hop address which is not reachable, we need this statement to allow them into the BGP table.
\end{description}


\appendix
\section{Upstream configuration}
Configuration of the ``upstream provider'':
\begin{verbatim}
hostname upstream
!
! Community-List to match BLACKHOLE
bgp community-list standard blackholing permit 65535:666
!
! Route for Blackholing
ip route 10.66.66.66/32 blackhole
ipv6 route 3000::66/128 blackhole
!
route-map customer-in permit 90
 match community blackholing
 set community no-export additive
 set ip next-hop 10.66.66.66
!
route-map customer-in permit 100
!
route-map customer-out deny 100
router bgp 65550
 no bgp default ipv4-unicast
 bgp ebgp-requires-policy
 ! for blackholing to work we need this
 bgp disable-ebgp-connected-route-check
 neighbor customer peer-group
 neighbor customer-v6 peer-group
 address-family ipv4 unicast
  neighbor customer activate
  neighbor customer route-map customer-in in
  neighbor customer route-map customer-out out
  neighbor customer soft-reconfiguration inbound
 exit-address-family
 address-family ipv6 unicast
  neighbor customer-v6 activate
  neighbor customer-v6 route-map customer-in in
  neighbor customer-v6 route-map customer-out out
  neighbor customer-v6 soft-reconfiguration inbound
 exit-address-family
 neighbor 10.200.1.2 remote-as 64501
 neighbor 10.200.1.2 peer-group customer
 neighbor 2001:db8:200:1::2 remote-as 64501
 neighbor 2001:db8:200:1::2 peer-group customer-v6
 ...
\end{verbatim}

\section{Initial Router Configuration}
Initially the router connects to AS65550
\begin{verbatim}
hostname rXX
!
interface dummy0
  ip address 172.16.x.1/32
  ip address 172.16.x.2/31
!
!
ip route 172.16.x.0/24 dummy0
ip prefix-list my-networks permit 172.16.x.0/24
!
interface eth1
!
interface eth2
 ip address 10.200.x.2/29
 ipv6 address 2001:db8:200:x::2/64
!
route-map upstream-in permit 100
 set local-preference 10
!
!
route-map upstream-out permit 50
 match ip address prefix-list my-networks
!
route-map upstream-out deny 100
!
!
route-map peering-in permit 100
 set local-preference 1000
!
route-map peering-out deny 100
!
router bgp 645xx
 no bgp default ipv4-unicast
 bgp ebgp-requires-policy
 neighbor upstream peer-group
 neighbor upstream-v6 peer-group
 neighbor peering peer-group
 neighbor peering-v6 peer-group
 address-family ipv4 unicast
  network 172.16.x.0/24
  neighbor upstream activate
  neighbor upstream route-map upstream-in in
  neighbor upstream route-map upstream-out out
  neighbor upstream soft-reconfiguration inbound
  neighbor peering soft-reconfiguration inbound
  neighbor peering route-map peering-in in
  neighbor peering route-map peering-out out
  neighbor peering next-hop-self
  neighbor peering activate
 exit-address-family
 address-family ipv6 unicast
  network 2001:db8:x::0/48
  neighbor upstream-v6 activate
  neighbor upstream-v6 route-map upstream-in in
  neighbor upstream-v6 route-map upstream-out out
  neighbor upstream-v6 soft-reconfiguration inbound
  neighbor peering-v6 activate
  neighbor peering-v6 soft-reconfiguration inbound
  neighbor peering-v6 route-map peering-in in
  neighbor peering-v6 route-map peering-out out
 exit-address-family
 neighbor 10.200.x.3 remote-as 65550
 neighbor 10.200.x.3 peer-group upstream
 neighbor 2001:db8:200:x::3 remote-as 65550
 neighbor 2001:db8:200:x::3 peer-group upstream-v6
!
\end{verbatim}

\section{Final Router Configuration}
Router config with blackholing solution applied
\begin{verbatim}
  hostname rXX
  !
  interface dummy0
    ip address 172.16.x.1/32
    ip address 172.16.x.2/31
  !
  !
  ip route 172.16.x.0/24 dummy0
  ip route 172.16.x.2 blackhole tag 666
  !
  ip prefix-list my-networks permit 172.16.x.0/24
  ip prefix-list my-networks-all permit 172.16.x.0/24 le 32
  !
  interface eth1
  !
  interface eth2
   ip address 10.200.x.2/29
   ipv6 address 2001:db8:200:x::2/64
  !
  route-map redistribute-static permit 100
   match tag 666
   set community blackhole additive
  !
  !
  route-map upstream-in permit 100
   set local-preference 10
  !
  route-map upstream-out permit 40
   match community is-blackholed
   match ip address prefix-list my-networks-all
  !
  route-map upstream-out permit 50
   match ip address prefix-list my-networks
  !
  route-map upstream-out deny 100
  !
  !
  route-map peering-in permit 100
   set local-preference 1000
  !
  route-map peering-out deny 100
  !
  router bgp 645xx
   no bgp default ipv4-unicast
   bgp ebgp-requires-policy
   neighbor upstream peer-group
   neighbor upstream-v6 peer-group
   neighbor peering peer-group
   neighbor peering-v6 peer-group
   address-family ipv4 unicast
    network 172.16.x.0/24
    redistribute static route-map redistribute-static
    neighbor upstream activate
    neighbor upstream route-map upstream-in in
    neighbor upstream route-map upstream-out out
    neighbor upstream soft-reconfiguration inbound
    neighbor peering soft-reconfiguration inbound
    neighbor peering route-map peering-in in
    neighbor peering route-map peering-out out
    neighbor peering next-hop-self
    neighbor peering activate
   exit-address-family
   address-family ipv6 unicast
    network 2001:db8:x::0/48
    redistribute static route-map redistribute-static
    neighbor upstream-v6 activate
    neighbor upstream-v6 route-map upstream-in in
    neighbor upstream-v6 route-map upstream-out out
    neighbor upstream-v6 soft-reconfiguration inbound
    neighbor peering-v6 activate
    neighbor peering-v6 soft-reconfiguration inbound
    neighbor peering-v6 route-map peering-in in
    neighbor peering-v6 route-map peering-out out
   exit-address-family
   neighbor 10.200.x.3 remote-as 65550
   neighbor 10.200.x.3 peer-group upstream
   neighbor 2001:db8:200:x::3 remote-as 65550
   neighbor 2001:db8:200:x::3 peer-group upstream-v6
  !
\end{verbatim}

\section{Slides}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Getting the attacked prefix into BGP}
\LARGE
\begin{itemize}
\item We want to blackhole 172.16.x.2/32
\item And need to get it into BGP for this
\end{itemize}

\vspace{0.5cm}
\begin{framed}
\begin{verbatim}
conf t

ip route 172.16.x.2/32 blackhole

router bgp 645xx
 address-family ipv4 unicast
  network 172.16.x.2/32

\end{verbatim}
\end{framed}

Some show commands:
\begin{itemize}
\item \textbf{show bgp ipv4} - check if 172.16.x.2/32 is visible
\item \textbf{show bgp ipv4 172.16.x.2} - check for BGP communities
\end{itemize}
\end{samepage}

\vspace{1cm}
\Huge
Is there anything missing?
%----------------------------------------------------------------
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Something is missing....}
\LARGE
\begin{itemize}
\item What is missing?
\item The \emph{blackhole} BGP community
\item We need to add the community somehow
\item The \emph{network} statement offers a \emph{route-map} parameter
\end{itemize}

\vspace{0.5cm}
\begin{framed}
\begin{verbatim}
conf t

route-map add-blackholing permit 100
 set community blackhole additive

router bgp 645xx
 address-family ipv4 unicast
  network 172.16.x.2/32 route-map add-blackholing

\end{verbatim}
\end{framed}

Some show commands:
\begin{itemize}
\item \textbf{show bgp ipv4} - check if 172.16.x.2/32 is visible
\item \textbf{show bgp ipv4 172.16.x.2} - check for BGP communities
\end{itemize}

This is not very elegant - it requires two statements. We will improve this later.

\end{samepage}
%----------------------------------------------------------------
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Announce blackholed prefix to upstream}
\LARGE

\subsubsection*{\LARGE Our current setup:}
\begin{verbatim}
ip prefix-list my-networks permit 172.16.x.0/24
route-map upstream-out permit 50
 match ip address prefix-list my-networks
\end{verbatim}

\subsubsection*{\LARGE Add a statement for allowing blackholed prefixes}
We use a community-list for matching:
\Large
\begin{framed}
\begin{verbatim}
bgp community-list standard is-blackholed permit blackhole
\end{verbatim}
\end{framed}
\LARGE
Try: ``show bgp ipv4 unicast community-list is-blackholed''

Add a statement for announcing:
\begin{framed}
\begin{verbatim}
route-map upstream-out permit 40
 match community is-blackholed
\end{verbatim}
\end{framed}
Try: ``show bgp ipv4 route-map upstream-out''

\subsubsection*{\LARGE Better: Have a 2nd allowlist}
\Large
\begin{framed}
\begin{verbatim}
ip prefix-list my-networks-all permit 172.16.x.0/24 le 32
!
route-map upstream-out permit 40
 match ip address prefix-list my-networks-all
 match community is-blackholed
\end{verbatim}
\end{framed}


\end{samepage}
%----------------------------------------------------------------
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Announce to upstream (complete)}
\LARGE
\begin{itemize}
\item We announce our standard prefixes
\item We allow blackholed prefixes also as small as /32s (for IPv4)
\end{itemize}

(do not enter this - we already have it configured)

\vspace{0.5cm}
\begin{verbatim}
ip prefix-list my-networks permit 172.16.x.0/24
ip prefix-list my-networks-all permit 172.16.x.0/24 le 32
!
bgp community-list standard is-blackholed permit blackhole
!
route-map upstream-out permit 40
  match community is-blackholed
  match ip address prefix-list my-networks-all
!
route-map upstream-out permit 50
 match ip address prefix-list my-networks
\end{verbatim}

\vspace{1.5cm}

Some show commands:
\begin{itemize}
\item \textbf{show bgp ipv4} - check if 172.16.x.2/32 is visible
\item \textbf{show bgp ipv4 172.16.x.2} - check for BGP communities
\end{itemize}
\end{samepage}
%----------------------------------------------------------------
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge More elegant solution}
\LARGE
\begin{itemize}
\item Currently we need two statements to initiate blackholing:
\begin{itemize}
  \item static route (\verb|ip route ....|)
  \item network statement in BGP address-family context
\end{itemize}
\item One statement should be enough
\item Solution: \emph{tag} static routes
\end{itemize}

\vspace{0.5cm}
First get rid of the network statement:
\Large
\begin{framed}
\begin{verbatim}
router bgp 645xx
 address-family ipv4 unicast
  no network 172.16.x.2/32 route-map add-blackholing
\end{verbatim}
\end{framed}

\LARGE
Change the static route and create redistribute route-map:
\begin{framed}
\begin{verbatim}
ip route 172.16.x.2/32 blackhole tag 666
!
route-map redistribute-static permit 100
  match tag 666
  set community blackhole additive
\end{verbatim}
\end{framed}

Add a redistribute clause to the BGP address-family context:
\begin{framed}
\begin{verbatim}
router bgp 645xx
 address-family ipv4 unicast
  redistribute static route-map redistribute-static
\end{verbatim}
\end{framed}


\end{samepage}
%----------------------------------------------------------------


\end{document}
