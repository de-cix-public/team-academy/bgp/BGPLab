#!/usr/bin/perl
#
#    Blackholing Experiment
#
#    - 1 * /30s to each for "upstream"
#    - 1 common /21 for "peering
#
#   IPs for Upstream:
#      - 10.200.$id.0/29
#
#    IPs for Peering:
#      - 80.81.192.1$id/21
#
#    AS Numbers:


use Getopt::Std;


getopts('r:t:n:h');

if ($opt_h || !$opt_n) {
  print <<EOF;
Usage: $0 -n <number of instances> -t [docker|clab]
EOF
  exit;
}

$opt_t = "docker" unless $opt_t;

if ($opt_t eq "docker") {
  open(OUT,">docker-compose.yml");

  print OUT <<EOF;
version: '2.4'
services:

  proxy:
    image: wtremmel/frr:latest
    hostname: upstream
    container_name: upstream
    sysctls:
                net.ipv6.conf.all.disable_ipv6: 0
                net.ipv6.conf.all.forwarding: 1
    cap_add:
            - ALL
    expose:
            - "179"
    ports:
      - "9099:80"
    volumes:
      - ./config/upstream:/etc/frr
    networks:
      admin:
EOF
  for ($i = 1; $i <= $opt_n; $i++) {
    my $nodestr = sprintf "%02d",$i;
    print OUT <<EOF;
      upstream$nodestr:
        ipv4_address: 10.200.$i.3
        ipv6_address: 2001:DB8:200:$i\::3
EOF
  }

  for ($i = 1; $i <= $opt_n; $i++) {
    my $nodestr = sprintf "%02d",$i;


    # setup chain of interfaces
    #
    my $myas = 64500 + $i;
    my $myhexas = sprintf("%x",$myas);

  print OUT <<EOF;

  r$nodestr:
    image: wtremmel/frr:latest
    hostname: r$nodestr
    container_name: r$nodestr
    sysctls:
      net.ipv6.conf.all.disable_ipv6: 0
      net.ipv6.conf.all.forwarding: 1
    cap_add:
            - ALL
    expose:
            - "179"
    ports:
      - "90$nodestr:80"
      - "20$nodestr:23"
    volumes:
      - ./config/r$nodestr:/etc/frr
    networks:
      upstream$nodestr:
        ipv4_address: 10.200.$i.2
        ipv6_address: 2001:DB8:200:$i\::2
        priority: 800
      peering:
        ipv4_address: 80.81.192.1$nodestr
        ipv6_address: 2001:7F8::$myhexas\:0:1
        priority: 200
      admin:
        priority: 1000

EOF
  }

print OUT <<EOF;
networks:
  admin:
    enable_ipv6: true
    ipam:
      config:
        - subnet: 172.22.0.0/16
        - subnet: 2001:DB8:1:1::/64
  peering:
    driver: bridge
    driver_opts:
      macvlan_mode: bridge
    enable_ipv6: true
    ipam:
      config:
        - subnet: 80.81.192.0/21
        - subnet: 2001:7F8::/64
EOF

  for ($i = 1; $i <= $opt_n; $i++) {
    printf OUT "  upstream%02d:\n",$i;
    print OUT  "    enable_ipv6: true\n";
    print OUT  "    ipam:\n";
    print OUT  "      config:\n";
    printf OUT "        - subnet: 10.200.%d.0/29\n",$i;
    printf OUT "        - subnet: 2001:DB8:200:$i\::/64\n",$i;
  }

  if (0) {

    printf "  peering:\n",$i;
    print  "    driver: macvlan\n";
    print  "    driver_opts: \n";
    print  "      macvlan_mode: bridge\n";
    print  "    enable_ipv6: true\n";
    print  "    ipam:\n";
    print  "      config:\n";
    printf "        - subnet: 80.81.192.0/21\n",$i;
    printf "        - subnet: 2001:7F8::/64\n",$i;
  }

  close OUT;
} elsif ($opt_t eq "clab") {
  open(OUT,">experiment.clab.yml");
  print OUT <<EOF;
name: experiment06
prefix: ""

topology:
  nodes:
    upstream:
      kind: linux
      image: wtremmel/frr:latest
      binds:
        - ./config/upstream:/etc/frr
      ports:
        - 9099:80
      exec:
EOF
  for ($i=1;$i<=$opt_n;$i++) {
    print OUT "        - \"ip address add dev eth$i 10.200.$i.3/29\"\n";
    print OUT "        - \"ip address add dev eth$i 2001:db8:200:$i\::3/64\"\n";
  }

  for ($i=1;$i<=$opt_n;$i++) {
    my $nodestr = sprintf "%02d",$i;
    my $myas = 64500 + $i;
    my $myhexas = sprintf("%x",$myas);

    print OUT <<EOF;

    r$nodestr:
      kind: linux
      image: wtremmel/frr:latest
      binds:
        - ./config/r$nodestr:/etc/frr
      ports:
        - 90$nodestr:80
        - 20$nodestr:23
      exec:
        - "ip address add dev eth1 10.200.$i.2/29"
        - "ip address add dev eth1 2001:db8:200:$i\::2/64"
        - "ip address add dev eth2 80.81.192.1$nodestr/21"
        - "ip address add dev eth2 2001:7f8::$myhexas\:0:1/64"
EOF
  }
  my $pport = $opt_n+1;

  print OUT <<EOF;
    peeringnet:
      kind: bridge

  links:
    - endpoints: ["upstream:eth$pport","peeringnet:upstream"]
EOF
  for ($i=1;$i<=$opt_n;$i++) {
    my $nodestr = sprintf "%02d",$i;
    print OUT <<EOF;
    - endpoints: ["r$nodestr:eth1","upstream:eth$i"]
    - endpoints: ["r$nodestr:eth2","peeringnet:port$nodestr"]
EOF

  }
  close OUT;
} else {
  die "choose either -t clab or -t docker"; 
}

#
# create upstream config
#
system("mkdir -p config/upstream");

# pinger script
open(OUT,">config/upstream/do-ping");
print OUT <<EOF;
#!/bin/sh

while true
do
EOF
for ($i = 1; $i <= $opt_n; $i++) {
  print OUT "  ping -c 1 172.16.$i.2 | grep \"bytes from\"\n";
}
print OUT "  sleep 10\n  echo -----\ndone\n";
close OUT;

# ---------------------------

  open(OUT,">config/upstream/frr.conf");
print OUT <<EOF;
hostname upstream
!
! Community-List to match BLACKHOLE
bgp community-list standard blackholing permit 65535:666
!
! Route for Blackholing
ip route 10.66.66.66/32 blackhole
ipv6 route 3000::66/128 blackhole
!
! Route for normal announcement
ip route 198.51.100.0/24 Null0
ip prefix-list my-networks permit 198.51.100.0/24 le 32
int dummy0
 ip address 198.51.100.48/31
!
! redistribute for outgoing blackholing
route-map redistribute-static permit 100
 match tag 666
 set community blackhole additive
!
route-map customer-in permit 90
 match community blackholing
 set community no-export additive
 set ip next-hop 10.66.66.66
!
route-map customer-in permit 100
!
!
route-map customer-out permit 50
 match ip address prefix-list my-networks
!
route-map customer-out deny 100
!
router bgp 65550
 no bgp default ipv4-unicast
 bgp ebgp-requires-policy
 ! for blackholing to work we need this
 bgp disable-ebgp-connected-route-check
 neighbor customer peer-group
 neighbor customer-v6 peer-group
 address-family ipv4 unicast
  network 198.51.100.0/24
  redistribute static route-map redistribute-static
  neighbor customer activate
  neighbor customer route-map customer-in in
  neighbor customer route-map customer-out out
  neighbor customer soft-reconfiguration inbound
 exit-address-family
 address-family ipv6 unicast
  neighbor customer-v6 activate
  neighbor customer-v6 route-map customer-in in
  neighbor customer-v6 route-map customer-out out
  neighbor customer-v6 soft-reconfiguration inbound
 exit-address-family
EOF

for ($i = 1; $i <= $opt_n; $i++) {
  my $nodestr = sprintf "%02d",$i;
  print OUT <<EOF;
 neighbor 10.200.$i.2 remote-as 645$nodestr
 neighbor 10.200.$i.2 peer-group customer
 neighbor 2001:db8:200:$i\::2 remote-as 645$nodestr
 neighbor 2001:db8:200:$i\::2 peer-group customer-v6
EOF
}
close OUT;


# ---------------------------

open(OUT,">config/upstream/shutdown-interfaces");
print OUT <<EOF;
conf t
EOF
for ($i = 1; $i <= $opt_n; $i++) {
  print OUT <<EOF;
  interface eth$i
    shutdown
EOF
}
print OUT <<EOF;
end
EOF
close OUT;

# ---------------------------

open(OUT,">config/upstream/noshutdown-interfaces");
print OUT <<EOF;
conf t
EOF
for ($i = 1; $i <= $opt_n; $i++) {
  print OUT <<EOF;
  interface eth$i
    no shutdown
    ipv6 address 2001:db8:200:$i\::3/64
EOF
}
print OUT <<EOF;
end
EOF
close OUT;

# ---------------------------

open(OUT,">config/upstream/setup-bfd");
print OUT <<EOF;
conf t
bfd
EOF
for ($i = 1; $i <= $opt_n; $i++) {
  print OUT <<EOF;
  peer 10.200.$i.2
  peer 2001:db8:200:$i\::2
EOF
}
print OUT <<EOF;
router bgp 65550
  neighbor customer bfd
  neighbor customer-v6 bfd
end
EOF
close OUT;

# ---------------------------
open(OUT,">config/upstream/setup-roles");
print OUT <<EOF;
conf t
no route-map customer-out deny 100
route-map customer-out permit 100
!
router bgp 65550
  neighbor customer local-role provider
  neighbor customer-v6 local-role provider
!
EOF
close OUT;

# ---------------------------

for ($i = 1; $i <= $opt_n; $i++) {
  my $nodestr = sprintf "%02d",$i;


  # setup chain of interfaces
  #
  my $myas = 64500 + $i;
  my $myhexas = sprintf("%x",$myas);
  my $peeras = 64500 + $i;

  # create and populate the volumes
  #
  system("mkdir -p config/r$nodestr");

  open(OUT,">config/r$nodestr/frr.conf");
print OUT <<EOF;
hostname r$nodestr
!
interface dummy0
  ip address 172.16.$i.1/32
  ip address 172.16.$i.2/31
!
!
ip route 172.16.$i.0/24 dummy0
ip prefix-list my-networks permit 172.16.$i.0/24
!
interface eth1
  ip address 10.200.$i.2/29
  ipv6 address 2001:db8:200:$i\::2/64
!
interface eth2
  ip address 80.81.192.1$nodestr/21
  ipv6 address 2001:7f8::$myhexas:0:1/64
!
route-map upstream-in permit 100
 set local-preference 10
!
!
route-map upstream-out permit 50
 match ip address prefix-list my-networks
!
route-map upstream-out deny 100
!
route-map peering-in permit 100
 set local-preference 1000
!
route-map peering-out deny 100
!
router bgp 645$nodestr
 no bgp default ipv4-unicast
 bgp ebgp-requires-policy
 neighbor upstream peer-group
 neighbor upstream-v6 peer-group
 neighbor peering peer-group
 neighbor peering-v6 peer-group
 address-family ipv4 unicast
  network 172.16.$i.0/24
  neighbor upstream activate
  neighbor upstream route-map upstream-in in
  neighbor upstream route-map upstream-out out
  neighbor upstream soft-reconfiguration inbound
  neighbor peering soft-reconfiguration inbound
  neighbor peering route-map peering-in in
  neighbor peering route-map peering-out out
  neighbor peering next-hop-self
  neighbor peering activate
 exit-address-family
 address-family ipv6 unicast
  network 2001:db8:$i\::0/48
  neighbor upstream-v6 activate
  neighbor upstream-v6 route-map upstream-in in
  neighbor upstream-v6 route-map upstream-out out
  neighbor upstream-v6 soft-reconfiguration inbound
  neighbor peering-v6 activate
  neighbor peering-v6 soft-reconfiguration inbound
  neighbor peering-v6 route-map peering-in in
  neighbor peering-v6 route-map peering-out out
 exit-address-family
 neighbor 10.200.$i.3 remote-as 65550
 neighbor 10.200.$i.3 peer-group upstream
 neighbor 2001:db8:200:$i\::3 remote-as 65550
 neighbor 2001:db8:200:$i\::3 peer-group upstream-v6
!
EOF

  close OUT;

  open(OUT,">config/r$nodestr/daemons");
print OUT <<EOF;
zebra=yes
bgpd=yes
ospfd=yes
ospf6d=yes
ripd=no
ripngd=no
isisd=yes
pimd=no
ldpd=no
nhrpd=no
eigrpd=no
babeld=no
sharpd=no
pbrd=no
bfdd=yes
fabricd=no
EOF
  close OUT;

  open(OUT,">config/r$nodestr/daemons.conf");
print OUT <<EOF;
#
# If this option is set the /etc/init.d/frr script automatically loads
# the config via "vtysh -b" when the servers are started.
# Check /etc/pam.d/frr if you intend to use "vtysh"!
#
vtysh_enable=yes
zebra_options="  -s 90000000 --daemon -A 127.0.0.1"
bgpd_options="   --daemon -A 127.0.0.1"
ospfd_options="  --daemon -A 127.0.0.1"
ospf6d_options=" --daemon -A ::1"
ripd_options="   --daemon -A 127.0.0.1"
ripngd_options=" --daemon -A ::1"
isisd_options="  --daemon -A 127.0.0.1"
pimd_options="  --daemon -A 127.0.0.1"
ldpd_options="  --daemon -A 127.0.0.1"
nhrpd_options="  --daemon -A 127.0.0.1"
eigrpd_options="  --daemon -A 127.0.0.1"
babeld_options="  --daemon -A 127.0.0.1"
sharpd_options="  --daemon -A 127.0.0.1"
pbrd_options="  --daemon -A 127.0.0.1"
staticd_options="  --daemon -A 127.0.0.1"
bfdd_options="  --daemon -A 127.0.0.1"
fabricd_options="  --daemon -A 127.0.0.1"

# The list of daemons to watch is automatically generated by the init script.
watchfrr_enable=yes
# watchfrr_options=(-d -r /usr/sbin/servicebBfrrbBrestartbB%s -s /usr/sbin/servicebBfrrbBstartbB%s -k /usr/sbin/servicebBfrrbBstopbB%s -b bB)

# If valgrind_enable is 'yes' the frr daemons will be started via valgrind.
# The use case for doing so is tracking down memory leaks, etc in frr.
valgrind_enable=no
valgrind=/usr/bin/valgrind
EOF
  close OUT;

  open(OUT,">config/r$nodestr/solution-06-01-blackhole");
  print OUT <<EOF;
conf t
ip route 172.16.$i.2/32 blackhole tag 666
!
ip prefix-list my-networks-all permit 172.16.$i.0/24 le 32
!
bgp community-list standard is-blackholed permit blackhole
!
route-map upstream-out permit 40
  match ip address prefix-list my-networks-all
  match community is-blackholed
!
route-map redistribute-static permit 100
  match tag 666
  set community blackhole additive
!
router bgp 645$nodestr
 address-family ipv4 unicast
  redistribute static route-map redistribute-static
 address-family ipv6 unicast
  redistribute static route-map redistribute-static

end

EOF
close OUT;

open(OUT,">config/r$nodestr/solution-06a-bfd");
  print OUT <<EOF;
conf t
bfd
peer 10.200.$i.3
peer 2001:db8:200:$i\::3
!
router bgp 645$nodestr
  neighbor upstream bfd
  neighbor upstream-v6 bfd
!
end

EOF
close OUT;

# ---------------------------
open(OUT,">config/r$nodestr/setup-roles");
my $myas = 64500 + $i;
my $myhexas = sprintf("%x",$myas);
print OUT <<EOF;
conf t
!
no route-map upstream-out deny 100
route-map upstream-out permit 100
!
no route-map peering-out deny 100
route-map peering-out permit 100
!
router bgp $myas
EOF
  for ($j = 1; $j <= $opt_n; $j++) {
    next if ($j == $i);
    my $nodestr = sprintf "%02d",$j;
    my $myas = 64500 + $j;
    my $myhexas = sprintf("%x",$myas);
    print OUT "neighbor 80.81.192.1$nodestr peer-group peering\n";
    print OUT "neighbor 80.81.192.1$nodestr remote-as $myas\n"; 
    print OUT "neighbor 2001:7f8::$myhexas:0:1 peer-group peering-v6\n";
    print OUT "neighbor 2001:7f8::$myhexas:0:1 remote-as $myas\n";
  }
  print OUT <<EOF;
end
EOF
  close OUT;
# ---------------------------
open(OUT,">config/r$nodestr/solution-roles");
my $myas = 64500 + $i;
my $myhexas = sprintf("%x",$myas);
print OUT <<EOF;
conf t
router bgp $myas
  neighbor upstream local-role customer
  neighbor upstream-v6 local-role customer
  neighbor peering local-role peer
  neighbor peering-v6 local-role peer
end
EOF
close OUT;




  system("chmod 777 config/r$nodestr");
  system("chmod 666 config/r$nodestr/*");

}

close EXA2;

system("cp config/r01/daemons* config/upstream");

system("chmod 777 config/upstream");
system("chmod 666 config/upstream/*");


