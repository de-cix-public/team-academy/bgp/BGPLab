\documentclass[a4paper,notitlepage,oneside,]{article}
\usepackage[a4paper,margin=2.54cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[sfdefault,scaled=.85]{FiraSans}
\usepackage{newtxsf}
\usepackage{framed}
%
\usepackage{hyperref}
\usepackage[acronym,toc]{glossaries}
\usepackage{parskip}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{float}
\usepackage{enumitem}
\usepackage{fancyhdr}
\newcommand{\rfc}[1]{\href{https://tools.ietf.org/html/rfc#1}{RFC{#1}}}
\begin{document}
\frenchspacing
\pagestyle{fancyplain}
\renewcommand{\footrulewidth}{0.4pt}
\setlength\headheight{20pt}
\fancyhf{}
\fancyhf[HLE,HLO]{\Huge Connect: http://go.de-cix.net/webinarlab}
\fancyhf[HRE,HRO,FRE,FRO]{\thepage}
\fancyhf[FLE,FLO]{DE-CIX Academy}
\fancyhf[FCE,FCO]{BGP for networks who peer}
\title{Setup iBGP}
\author{\href{mailto:academy@decix.net}{DE-CIX Academy}}
\date{Version 1.0w}

%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Connecting}
\LARGE
\begin{itemize}
  \item You only need a web browser
  \item The URL to connect to see above - you will be redirected to a router
  \item All routers run on the same server
  \item They run on different \emph{ports} - port \textbf{9001} is router r01 and so on
  \item Connect to your router now
\end{itemize}

\subsection*{\Huge Entering commands}
\begin{itemize}
  \item Your router has two modes of operation
  \item \textbf{Terminal Mode} - this is the one you start with.
  \item The prompt in terminal mode is your router name followed by ``\#''
  \item Your router can \emph{autocomplete} commands - just use the \emph{tab} key (type twice for a list of possible completions).
  \item Or type ``?'' for a list of possible things you can enter
\end{itemize}

\vspace{1cm}

Lets try that now!

\begin{framed}
\begin{verbatim}
r01# sh?
\end{verbatim}
\end{framed}

\end{samepage}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Setup iBGP for IPv4}
\LARGE
\begin{itemize}
  \item We use \emph{peer-groups} for configuration
  \item All \emph{common} config statements go into the peer-group
  \item All individual statements to into the peer entry
\end{itemize}
\vspace{1cm}

\begin{framed}
\begin{verbatim}
  router bgp 64500
   no bgp default ipv4-unicast

   neighbor internal peer-group
   neighbor internal remote-as 64500
   neighbor internal update-source dummy0
   address-family ipv4 unicast
     neighbor internal activate
   exit-address-family

   neighbor 172.16.1.yy peer-group internal
   ...
\end{verbatim}
\end{framed}

\begin{itemize}
  \item Repeat the last line for all neighbors
  \item For \emph{yy} use the router ids of the other routers (all except your own)
  \item We change the default so IPv4 is not automatically activated
\end{itemize}

Show commands:
\begin{itemize}
  \item show bgp ipv4 summary
  \item show bgp ipv4 neighbors
  \item show bgp ipv4 neighbor 172.16.1.yy
\end{itemize}
\end{samepage}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Setup iBGP for IPv6}
\LARGE
\begin{itemize}
  \item We again use a \emph{peer-group}
  \item You need separate peer-groups for IPv4 and IPv6
  \item All \emph{common} config statements go into the peer-group
  \item All individual statements to into the peer entry
\end{itemize}
\vspace{0.5cm}

\begin{framed}
\begin{verbatim}
router bgp 64500
 no bgp default ipv4-unicast

 neighbor internal-v6 peer-group
 neighbor internal-v6 remote-as 64500
 neighbor internal-v6 update-source dummy0
 address-family ipv6 unicast
  neighbor internal-v6 activate
 exit-address-family

 neighbor 2001:db8:500::1:yy peer-group internal-v6
 ...
\end{verbatim}
\end{framed}

\begin{itemize}
  \item See the \emph{address-family} entry?
  \item This is about what is being \emph{transported} via BGP
  \item We have to activate IPv6
  \item and deactivate (\emph{no bgp\ldots}) IPv4 (we simply change the default)
\end{itemize}

Show commands:
\begin{itemize}
  \item show bgp ipv6 summary
  \item show bgp ipv6 neighbors
  \item show bgp ipv6 neighbor 2001:db8:500::1:yy
\end{itemize}
\end{samepage}
%----------------------------------------------------------------


%----------------------------------------------------------------

\clearpage
\fancyhf[HLE,HLO]{Setup iBGP}
\maketitle
\section{Purpose}
iBGP is simply BGP within one Autonomous System. It is used to redistribute prefixes received from other providers and to redistribute your own or your customers prefixes.

\section{Network Setup}
\begin{figure}[H]
  \centering
  \includegraphics[width=\linewidth,page=2]{img/01-ibgp-drawings.pdf}
  \caption{Network Setup}
  \label{fig:networksetup}
\end{figure}
Figure \ref{fig:networksetup} shows the network topology for this experiment. All devices are connected in a ring like structure, each device has two neighbors. Loopback addresses are distributed via an IGP (OSPF or IS-IS).

\section{Setup iBGP}
\subsection{Tasks:}
\begin{itemize}
  \item Define iBGP peer groups for IPv4 and IPv6
  \item Configure all parameters needed in the peer group
  \item Set up iBGP sessions to all other routers on IPv4 and IPv6 using the peer groups
  \item Check if the sessions are up
\end{itemize}

\subsection{To set up iBGP you need to:}
\begin{itemize}
  \item start BGP using your AS number
  \item define one iBGP peer group for IPv4 and one for IPv6
  \item configure all iBGP neighbors
\end{itemize}

Information you need:
\begin{itemize}
  \item Your AS number: \textbf{64500}
\end{itemize}
The IP addresses of your iBGP neighbors you can find out by doing a \emph{show ip route ospf} or \emph{show ip route isis} (depending on the IGP in use) and look for /32 prefixes for IPv4. For IPv6 do a \emph{show ipv6 route ospf6} or \emph{show ipv6 route isis} and look for /128 prefixes.

\subsection{To configure iBGP in config mode you need:}
\begin{description}
  \item[router bgp <asnumber>] to start BGP with \emph{<asnumber>} as your AS number
  \item[neighbor <name> peer-group] to start configuring a peergroup named \emph{<name>}
  \item[neighbor <name> remote-as <asnumber>] to set a common remote AS number for all peer group members. If configuring iBGP \emph{<asnumber>} is \textbf{your} AS number.
  \item[neighbor <name> update-source <interface>] make BGP use the IP address of \emph{<interface>} as source IP when setting up connections. Use your loopback interface name (\emph{dummy0}) here.
  \item[neighbor <name> next-hop-self] makes iBGP to set this routers IP (of \emph{dummy0}) for next-hop address when distributing prefixes received via eBGP
  \item[neighbor <name> next-hop-self all] makes iBGP to set this routers IP (of \emph{dummy0}) for next-hop address when distributing prefixes received via eBGP \emph{or iBGP}
  \item[neighbor <name> send-community both] to forward BGP communities. We have not covered them yet, but you will need this later on.
  \item[neighbor <ip address> peer-group <name>] sets up a BGP session to \emph{<ip address>} as a member of peer-group \emph{<name>}. That means all configs from \emph{<name>} are copied.
  \item[address-family ipv6] switches into IPv6 context. You need to activate explicitly distributing IPv6 prefixes in this context.
  \item[address-family ipv4] switches into IPv4 context. You do not need to activate the distribution of IPv4 prefixes via IPv4 - this is activated by default. But you need to \emph{de}activate the distribution of IPv6 addresses.
  \item[neighbor <ipv6 address> activate] in address-family context  activates distribution of IPv6 prefixes in this context.
  \item[neighbor <ipv4 address> activate] in address-family context activates distribution of IPv4 prefixes. Not needed in IPv4 context.
  \item[no neighbor <ipv6 address> activate] in address-family context  prevents distribution of IPv6 prefixes.
  \item[no neighbor <ipv4 address> activate] in address-family context  prevents distribution of IPv4 prefixes.
\end{description}
\pagebreak[1]

It is recommended to distribute prefixes only in their own context. So you usually configure:
\begin{verbatim}
  ...
  address-family ipv4 unicast
    neighbor <ipv4 address> activate
    ... (for all IPv4 neighbors)
    no neighbor <ipv6 address> activate
    ... (for all IPv6 neighbors)
  exit-address-family
  address-family ipv6 unicast
    no neighbor <ipv4 address> activate
    ... (for all IPv4 neighbors)
    neighbor <ipv6 address> activate
    ... (for all IPv6 neighbors)
  exit-address-family
\end{verbatim}
In FRRouting you can configure this also for a whole peer group and you can change the default so IPv4 is \emph{not} activated automatically. See solution below.

\subsection{Commands to check if iBGP is running:}
\begin{description}
  \item[show ip bgp summary] shows you all configured neighbor and if sessions to them are up or not.
  \item[show ip bgp neighbors] show you detailed information about all BGP neighbors (long!)
  \item[show ip bgp neigbors <ip address>] same as above, but only for one neigbor. Preferred.
\end{description}

\section{Solution}
\subsection{IPv4}
\begin{verbatim}
  router bgp 64500
    no bgp default ipv4-unicast
    neighbor internal peer-group
    neighbor internal remote-as 64500
    neighbor internal update-source dummy0
    address-family ipv4 unicast
      neighbor internal activate
    exit-address-family
    neighbor 172.16.1.YY peer-group internal
    ...
\end{verbatim}
Where \emph{YY} are the IPv4 addresses of all other routers in the network (except your own).

\subsection{IPv6}
\begin{verbatim}
  router bgp 64500
    no bgp default ipv4-unicast
    neighbor internal-v6 peer-group
    neighbor internal-v6 remote-as 64500
    neighbor internal-v6 update-source dummy0
    address-family ipv6 unicast
      neighbor internal-v6 activate
    exit-address-family
    neighbor 2001:db8:500::1:Y peer-group internal-v6
    ...
  \end{verbatim}

Where \emph{Y} are the IPv6 addresses of all other routers in the network (except your own).

\section{Links mentioned in the webinar}
\subsection{RFCs}
\begin{itemize}
  \item \href{https://tools.ietf.org/html/rfc1930}{RFC1930} - Autonomous Systems
  \item \href{https://tools.ietf.org/html/rfc4271}{RFC4271} - BGP
  \item \rfc{4456} - Route Reflectors
\end{itemize}

\subsection{Software}
\begin{itemize}
  \item \href{https://bitbucket.org/decix-academy/dockerbgp/src/master/}{DE-CIX Academy Lab}
  \item \href{https://frrouting.org}{FRRouting}
  \item \href{https://www.gns3.com}{GNS3}

\end{itemize}




\end{document}
