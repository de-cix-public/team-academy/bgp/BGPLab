\documentclass[a4paper,notitlepage,oneside,]{article}
\usepackage[a4paper,margin=2.54cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[sfdefault,scaled=.85]{FiraSans}
\usepackage{newtxsf}
\usepackage{framed}
%
\usepackage{hyperref}
\usepackage[acronym,toc]{glossaries}
\usepackage{parskip}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{float}
\usepackage{enumitem}
\usepackage{fancyhdr}
\begin{document}
\frenchspacing
\pagestyle{fancyplain}
\renewcommand{\footrulewidth}{0.4pt}
\fancyhf{}
\fancyhf[HLE,HLO]{Experiment 1e - Distribute prefixes}
\fancyhf[HRE,HRO,FRE,FRO]{\thepage}
\fancyhf[FLE,FLO]{DE-CIX Academy}
\fancyhf[FCE,FCO]{BGP for networks who peer}
\title{Experiment 1e - Distribute prefixes}
\date{Version 1.2}
\author{\href{mailto:academy@decix.net}{DE-CIX Academy}}
\maketitle
\section{Introduction}
BGP is all about prefixes. To get prefixes \emph{into} BGP there are two methods, in this experiment we will use the simplest method adding prefixes using a \emph{network} statement.

\section{Network Setup}
\begin{figure}[htp]
  \centering
  \includegraphics[width=\linewidth,page=2]{img/01-ibgp-drawings.pdf}
  \caption{Network Setup}
  \label{fig:networksetup}
\end{figure}
Figure \ref{fig:networksetup} shows the network topology for this experiment. All devices are connected in a ring like structure, each device has two neighbors. Loopback addresses are distributed via an IGP (OSPF or IS-IS).

iBGP has already been set up between the loopback addresses of the routers.

\section{Distribute prefixes}
\subsection{Tasks:}
\begin{itemize}
  \item Put in static routes for one IPv4 and one IPv6 prefix
  \item Distribute these prefixes in BGP
\end{itemize}

\subsection{To distribute prefixes you need to:}
\begin{itemize}
  \item have the prefix to be distributed in your routing table
  \item tell BGP to distribute that prefix
\end{itemize}

Information you need:
\begin{itemize}
  \item A couple of prefixes to distribute
  \item Have a look at the last part of your loopback address. If your loopback address is for example 172.16.1.\textbf{X}/32, then you can use
  \begin{itemize}
    \item 10.\textbf{X}.0.0/16
    \item 2001:db8:\textbf{X}::/48
  \end{itemize}
    to redistribute
\end{itemize}

\subsection{To configure routes and redistribution in config mode:}
\begin{description}
  \item[ipv6 route <prefix> <destination>]
  \item[ip route <prefix> <destination>] This creates static routes in your routing table for prefix \emph{<prefix>}. \emph{<destination>} is where packets to this prefix are sent, if you want just a static route so you can put the prefix into BGP you can use either the \emph{Null0} or your \emph{dummy0} interface as destination.
  \item[address-family ipv4 unicast] or
  \item[address-family ipv6 unicast] in BGP context selects an adress family for the following \emph{network} config commands
  \item[network <prefix>] in BGP address-family context tells your BGP router to put \emph{<prefix>} into BGP \textbf{if there is a corresponding route to it in your routing table}
  \item[network <prefix> route-map <name>] same as above, but uses route-map \emph{<name>} to modify attributes before putting it into BGP. We will cover this later.
\end{description}


\subsection{Commands to check if your prefix is announced:}
\begin{description}
  \item[show ipv6 bgp]
  \item[show ip bgp] lists all prefixes in the BGP table
  \item[show ipv6 bgp \emph{<prefix>}]
  \item[show ip bgp \emph{<prefix>}] gives you detailed information about \emph{<prefix>} including to which neigbors it is announced.

\end{description}

\section{Distribution of prefixes learned via eBGP}
We did not configure eBGP yet, but it is important to learn how prefixes received via eBGP are distributed in iBGP. By default, all prefixes leared via eBPG are distributed \emph{unchanged}, that means all attributes and also the next-hop IP address stay as they are received.

Especially for the next-hop address this can be a problem, if you do not distributed all interface addresses via your IGP. Thankfully there is a solution for that. You can configure BGP to replace the next-hop address with the IP address of the distributing iBGP router.

\subsection{To configure next-hop address behaviour:}
This is configured in \emph{address-family} context.
\begin{description}
  \item[neighbor <name> next-hop-self] makes iBGP to set this routers IP (of \emph{dummy0}) for next-hop address when distributing prefixes received via eBGP
  \item[neighbor <name> next-hop-self all] makes iBGP to set this routers IP (of \emph{dummy0}) for next-hop address when distributing prefixes received via eBGP \emph{or iBGP}
\end{description}


\section{Solution}
\subsection{IPv4}
\begin{verbatim}
  ip route 10.something.somethingelse.0/24 Null0
  router bgp 64500
    network 10.something.somethingelse.0/24
\end{verbatim}
Choose any valid prefix for this. Ask your neighbors if the see the prefix in the BGP and routing tables.

\subsection{IPv6}
\begin{verbatim}
  ipv6 route 2001:db8:something::/48 Null0
  router bgp 64500
    address-family IPv6 unicast
      network 2001:db8:something::/48
\end{verbatim}
Here the \emph{network} statement has to be within the \emph{address-family} context. The only reason you can define an IPv4 statement without the context is backward compatibility.

\section{Slides}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Distribute an IPv4 prefix}
\LARGE
\begin{itemize}
  \item any prefix you want to distribute must be in the routing table first
  \item so you do need a static route entry for it
\end{itemize}
\vspace{1cm}

\begin{framed}
\begin{verbatim}
  ip route 10.11.xx.0/24 null0

  router bgp 64500
   address-family ipv4 unicast
    network 10.11.xx.0/24

\end{verbatim}
\end{framed}

\begin{itemize}
  \item For \emph{xx} use your router id (01, 02, \ldots)
  \item Instead of 10.11.xx.0/24 you can also announce any other prefix
  \item A \emph{route target} of ``null0'' means ``we have no target for this prefix but want it in our routing table anyway''
\end{itemize}

Show commands:
\begin{itemize}
  \item show ip route static
  \item show bgp ipv4
  \item show bgp ipv4 10.11.xx.0/24
  \item show ip route bgp
\end{itemize}
\end{samepage}
%----------------------------------------------------------------
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Distribute an IPv6 prefix}
\LARGE
\begin{itemize}
  \item You also need a static route for your IPv6 prefix
  \item Be aware of the address-family statement
\end{itemize}
\vspace{1cm}

\begin{framed}
\begin{verbatim}
  ipv6 route 2001:db8:xx:yyyy::/48 Null0

  router bgp 64500
   address-family ipv6
    network 2001:db8:xx:yyyy::/48

\end{verbatim}
\end{framed}

\begin{itemize}
  \item For \emph{xx} use your id (01, 02, \ldots)
  \item You can also use any other IPv6 prefix for this
  \item A \emph{route target} of ``null0'' means ``we have no target for this prefix but want it in our routing table anyway''
\end{itemize}

Show commands:
\begin{itemize}
  \item show ipv6 route static
  \item show bgp ipv6
  \item show bgp ipv6 2001:db8:xx:yyyy::/48
  \item show ipv6 route bgp
\end{itemize}
\end{samepage}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Change the next-hop address}
\LARGE
\begin{itemize}
  \item Check your BGP table - show bgp ipv4
  \item The prefix \emph{r01} distributes is
  \emph{inaccessible} by the other routers
  \item because the next hop it has is not distributed
  \item \emph{r01} needs to set itself as the next-hop for iBGP
\end{itemize}
\vspace{1cm}

\begin{framed}
\begin{verbatim}
  router bgp 64500
   address-family ipv4
    neighbor internal next-hop-self
   exit-address-family
   address-family ipv6
    neighbor internal-v6 next-hop-self
   exit-address-family
\end{verbatim}
\end{framed}

Show commands:
\begin{itemize}
  \item show bgp ipv4
  \item show bgp ipv6
\end{itemize}
\end{samepage}


\end{document}
