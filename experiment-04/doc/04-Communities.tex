\documentclass[a4paper,notitlepage,oneside,]{article}
\usepackage[a4paper,margin=2.54cm]{geometry}
\usepackage[T1]{fontenc}
\usepackage[sfdefault,scaled=.85]{FiraSans}
\usepackage{newtxsf}
\usepackage{framed}
%
\usepackage{hyperref}
\usepackage[acronym,toc]{glossaries}
\usepackage{parskip}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage{colortbl}
\usepackage{float}
\usepackage{enumitem}
\usepackage{fancyhdr}
\begin{document}
\frenchspacing
\pagestyle{fancyplain}
\renewcommand{\footrulewidth}{0.4pt}
\fancyhf{}
\fancyhf[HLE,HLO]{Experiment 4 - Working with BGP communities}
\fancyhf[HRE,HRO,FRE,FRO]{\thepage}
\fancyhf[FLE,FLO]{DE-CIX Academy}
\fancyhf[FCE,FCO]{BGP for networks who peer}
\title{Experiment 4 - Working with BGP communities}
\date{Version 1.1 - Students Copy}
\author{\href{mailto:academy@decix.net}{DE-CIX Academy}}
\maketitle
\section{Introduction}
With BGP Communities you can add information to your prefixes. As communities itself are ``just numbers'', you need to define yourself what these numbers mean to you. In this experiment we will work with the different flavors of BGP communities (standard and extended).

\section{Network Setup}
\begin{figure}[htp]
  \centering
  \includegraphics[width=\linewidth,page=1]{img/04-communities-drawings.pdf}
  \caption{Network Setup}
  \label{fig:networksetup}
\end{figure}
Figure \ref{fig:networksetup} shows the network topology for this experiment.
Two upstreams, one customer and everybody peers with each other.

\section{Adding Informational Communities}
To add communities to incoming prefixes you need to:
\begin{itemize}
  \item Define a list of communities and their meaning
  \item Apply the communities at the entry of prefixes to your network
\end{itemize}

\subsection{Task}
Add informational communities to all prefixes in your routing table. The following information must be encoded:
\begin{itemize}
  \item Received via peering, upstream or customer
  \item Location where prefix was received
  \item Router where prefix was received
\end{itemize}
Keep in mind that in a later task you must also:
\begin{itemize}
  \item Define communities where you allow customers to influence routing
  \item Define communities to command your router to announce / not announce prefixes.
\end{itemize}
So what ever you define, leave space for expansion.

\section{Community lists}
To define lists of communities you can use community lists. There are lists for standard, extended and large communities. And for all three a standard or expanded type:
\begin{description}
  \item[Standard] type community lists do their matching on (complete) community numbers (like 64500:122).
  \item[Expanded] community lists do their matching on regular expressions. That means that the communities to be checked are not seen as numbers but as strings. The usual regular expression matching can be used:\\
  \begin{tabular}{cl}
    \^{} & matches start of string \\
    \$ & matches end of string \\
    \verb|[]| & range of characters like [1-3] \\
    . & any single character \\
    \verb|_| & underscore matches any whitespace, beginning or end of line, comma \\
    \verb|*| & matches zero or more occurrences of the pattern before \\
    \verb|+| & matches one or more occurrences of the pattern before \\
    \verb|?| & matches zero or one occurrences of the pattern before

  \end{tabular}
\end{description}
Both list types can have \emph{permit} and \emph{deny} lines, lists are parsed sequentially and first match terminates. Result is the type of the line (permit or deny).

\subsubsection{Examples}
Match communities 64500:10000 or 64500:11000 (one of them must be there):
As standard list:
\begin{verbatim}
  bgp community-list standard Example permit 64500:10000
  bgp community-list standard Example permit 64500:11000
  bgp community-list standard Example deny
\end{verbatim}
The last line is not really necessary as there is an implicit deny in FRRouting, but that might not be the case in every router software.

Same as expanded list:
\begin{verbatim}
  bgp community-list expanded Example permit _64500:1[01]000
\end{verbatim}
So you see that using expanded lists your lists can be shorter (but sometimes also less readable).
Also expanded community lists take up more CPU cycles to process. Still, most of the time they are more useful then standard lists.

Match my own communities (64500:.*) where
\begin{itemize}
  \item the first digit is 4
  \item second digit is 2,3,6 or 7
  \item third to last is \emph{not} 840
\end{itemize}
This can be used to filter outgoing prefixes, like announce to peers, but not announce in the US.
As expanded list:
\begin{verbatim}
  bgp community-list expanded PeeringOutNotUSA deny _64500:4[2367]840_
  bgp community-list expanded PeeringOutNotUSA permit _64500:4[2367]..._
\end{verbatim}

As standard list:
\begin{verbatim}
  bgp community-list standard PeeringOutNotUSA deny 64500:42840
  bgp community-list standard PeeringOutNotUSA deny 64500:43840
  bgp community-list standard PeeringOutNotUSA deny 64500:46840
  bgp community-list standard PeeringOutNotUSA deny 64500:47840
  bgp community-list standard PeeringOutNotUSA permit 64500:42000
  bgp community-list standard PeeringOutNotUSA permit 64500:43000
  bgp community-list standard PeeringOutNotUSA permit 64500:46000
  bgp community-list standard PeeringOutNotUSA permit 64500:47000
  bgp community-list standard PeeringOutNotUSA permit 64500:42001
  ...
\end{verbatim}
You see, this does not really work. So if you want to work with some wildcards, you need expanded lists (not all routers have them!).

\section{Config statements you may need}
\subsection{Community lists}
\begin{description}
  \item[bgp community-list (<1-99>|standard <name>) (permit|deny) <AA:NN>\ldots]
  Adds lines to a standard community list for orignal communities.
  \item[bgp community-list (<300-500>|expanded <name>) (permit|deny) <regular expression>] adds a line with a regular expression to an expanded community list.
  \item[bgp extcommunity-list (<1-99>|standard <name>) (permit|deny) (rt|soo) (<aaaa:nn>|<aa:nnnn>)\ldots] adds a \emph{route target} or \emph{site of origin} extended community rule to a standard list. One of \emph{<aaaa>} or \emph{<nnnn>} can be 32 bits long.
  \item[bgp extcommunity-list (<100-500>|expanded <name>) (permit|deny) <regular expression>] defines a line in an expanded list for extended communities.
  \item[bgp large-community-list (<1-99>|standard <name>) (permit|deny) <aaaa:bbbb:cccc>\ldots] adds a line to a standard large-community list. All of \emph{<aaaa>}, \emph{<bbbb>} and \emph{<cccc>} are 32 bits long.
  \item[bgp large-community-list (<100-500>|expanded <name>) (permit|deny) <regular expression>] defines a line in an expanded community list for large communities.
\end{description}
Note that standard and expanded lists share the same name-space, so you the \emph{<name>} you are using must be unique.


\subsection{Within route-maps}
\subsubsection{Set statements}
\begin{description}
  \item[set communitity <AA:NN>\ldots] Clears all existing and sets a list of communities.
  \item[set communitiy <AA:NN>\ldots additive] Keeps existing communities in place and simply adds the listed communities. \emph{AA} is a 16-bit AS number, \emph{NN} some 16-bit number.
  \item[set extcommunity rt ASN:NN\ldots] Clears all existing extended communities and sets new ones. There is no ``additive'' command for extended communties. \emph{ASN} is a 32-bit AS number, \emph{NN} some 16-bit number.
  \item[set large-community AA:BB:CC\ldots \emph{[additive]}] Similar to the original communities, you can set or add (using \emph{additive} keyword) large communities. All of \emph{AA,BB,CC} are 32-bit numbers.
\end{description}

\subsubsection{Match statements}
In route-maps matches against communities attached to prefix announcements can be made. For this, you need to define a community list first (can be any list, standard or expanded) and then match against this list:
\begin{description}
  \item[match community \emph{<listname>}] parses the list named \emph{<listname>} and checks if one of the communities attached to the prefix matches.
  \item[match community \emph{<listname>} exact-match] matches only if there is an exact match between the prefix and the community-list.
  \item[match extcommunity \emph{<listname>}] same matching for extended communities. There is not ``exact-match'' keyword here.
  \item[match large-community \emph{<listname>}] same for large community lists. Also no ``exact-match'' keyword here.
\end{description}

\subsubsection{Deleting communities}
For removing communities from prefix announcements also route-maps have to be used. The communities to be removed have to be defined in a community-list (standard or expanded):
\begin{description}
  \item[set comm-list \emph{<listname>} delete]
  \item[set large-comm-list \emph{<listname>} delete] removes all communities matching the list ``<listname>''.
\end{description}
There is no command to remove extended communities.

\section{show commands}
\subsubsection{community lists}
\begin{description}
  \item[show bgp (community-list|extcommunity-list|large-community-list) [<1-500>|<name>] ] shows either all or a named or numbered community list.
\end{description}

\subsubsection{Prefixes}
\begin{description}
  \item[show bgp (ipv4|ipv6) community <aa:nn>\ldots [exact-match] ] list prefixes with communities \emph{<aa:nn>} attached to them.
  \item[show bgp (ipv4|ipv6) large-community <aaaa:bbbb:cccc>\ldots [exact-match] ] list prefixes with large communities \emph{<aaaa:bbbb:cccc>} attached to them.
  \item[show bgp (ipv4|ipv6) community-list (<1-500>|<name>) [exact-match] ] shows bgp prefixes matching the standard or expanded community list \emph{<name>}.
  \item[show bgp (ipv4|ipv6) large-community-list (<1-500>|<name>)] shows bgp prefixes matching the standard or expanded large community list \emph{<name>}.
\end{description}

\section{Task: Control announcement of prefixes using communities}
You receive prefixes from four different kinds of sources:
\begin{itemize}
  \item From your customer, AS64499
  \item From your upstream providers
  \item From a number of peerings
  \item From yourself, your own netblock that you announce
\end{itemize}

Your task is to implement a pretty standard routing policy:
\begin{itemize}
  \item Announce your own and your customers prefixes to all upstreams and peers
  \item Announce prefixes you receive from upstream and peers to your customer only.
\end{itemize}

For this you need to:
\begin{enumerate}
  \item Define a list of communities to control announcement of prefixes (keep in mind that any combination of announcing to peering, upstream or customer must be possible)
  \item Tag all prefixes in your routing table with the needed community/ies.
  \item Filter all announcements to the outside via eBGP according to the communities set.
\end{enumerate}


\section{Task: Allow your customer to control your announcement}
As a good transit provider you offer your customers the control over their prefixes. Define and implement the following using BGP communities:
\begin{itemize}
  \item Allow customers to control announcing their prefixes to upstream, peering or other customers (or and combination of them)
  \item Prevent customers from sending you any other communities except the ones you allow them for announcement control.
\end{itemize}

\appendix
\section{Initial Router Configuration}
\begin{verbatim}
  hostname rXX
  !
  ip route 10.XX.0.0/16 Null0
  !
  interface dummy0
   ip address 10.XX.1.1/32
  !
  router bgp 645XX
   neighbor customer peer-group
   neighbor peering peer-group
   neighbor upstream peer-group
   neighbor 10.XX.2.1 remote-as 64499
   neighbor 10.XX.2.1 peer-group customer
   neighbor 80.81.192.1 remote-as 286
   neighbor 80.81.192.1 peer-group peering
   neighbor 80.81.192.102 remote-as 64502
   neighbor 80.81.192.102 peer-group peering
   neighbor 80.81.192.103 remote-as 64503
   neighbor 80.81.192.103 peer-group peering
   neighbor 80.81.192.104 remote-as 64504
   neighbor 80.81.192.104 peer-group peering
   neighbor 80.81.192.105 remote-as 64505
   neighbor 80.81.192.105 peer-group peering
   neighbor 80.81.192.106 remote-as 64506
   neighbor 80.81.192.106 peer-group peering
   neighbor 10.200.XX.1 remote-as 65550
   neighbor 10.200.XX.1 peer-group upstream
   neighbor 10.230.XX.1 remote-as 64496
   neighbor 10.230.XX.1 peer-group upstream
   !
   address-family ipv4 unicast
    network 10.XX.0.0/16
    neighbor customer next-hop-self
    neighbor customer soft-reconfiguration inbound
    neighbor customer route-map customer-in in
    neighbor customer route-map customer-out out
    neighbor peering next-hop-self
    neighbor peering soft-reconfiguration inbound
    neighbor peering route-map peering-in in
    neighbor peering route-map peering-out out
    neighbor upstream next-hop-self
    neighbor upstream soft-reconfiguration inbound
    neighbor upstream route-map upstream-in in
    neighbor upstream route-map upstream-out out
 exit-address-family
!
bgp as-path access-list my-as permit ^$
!
route-map upstream-out permit 100
 match as-path my-as
!
route-map upstream-in permit 100
 set local-preference 10
!
route-map peering-out permit 100
 match as-path my-as
!
route-map peering-in permit 100
 set local-preference 1000
!
route-map customer-out permit 100
!
route-map customer-in permit 100
 set local-preference 10000
!
end
\end{verbatim}

\section{Final Router Configuration}
\begin{verbatim}
hostname rXX
!
ip route 10.XX.0.0/16 Null0
!
interface dummy0
 ip address 10.XX.1.1/32
!
router bgp 645XX
 neighbor customer peer-group
 neighbor peering peer-group
 neighbor upstream peer-group
 neighbor 10.XX.2.1 remote-as 64499
 neighbor 10.XX.2.1 peer-group customer
 neighbor 80.81.192.1 remote-as 286
 neighbor 80.81.192.1 peer-group peering
 neighbor 80.81.192.102 remote-as 64502
 neighbor 80.81.192.102 peer-group peering
 neighbor 80.81.192.103 remote-as 64503
 neighbor 80.81.192.103 peer-group peering
 neighbor 80.81.192.104 remote-as 64504
 neighbor 80.81.192.104 peer-group peering
 neighbor 80.81.192.105 remote-as 64505
 neighbor 80.81.192.105 peer-group peering
 neighbor 80.81.192.106 remote-as 64506
 neighbor 80.81.192.106 peer-group peering
 neighbor 10.200.XX.1 remote-as 65550
 neighbor 10.200.XX.1 peer-group upstream
 neighbor 10.230.XX.1 remote-as 64496
 neighbor 10.230.XX.1 peer-group upstream
 !
 address-family ipv4 unicast
  network 10.XX.0.0/16 route-map own-networks
  neighbor customer next-hop-self
  neighbor customer soft-reconfiguration inbound
  neighbor customer route-map customer-in in
  neighbor customer route-map customer-out out
  neighbor peering next-hop-self
  neighbor peering soft-reconfiguration inbound
  neighbor peering route-map peering-in in
  neighbor peering route-map peering-out out
  neighbor upstream next-hop-self
  neighbor upstream soft-reconfiguration inbound
  neighbor upstream route-map upstream-in in
  neighbor upstream route-map upstream-out out
 exit-address-family
!
bgp as-path access-list my-as permit ^$
!
bgp community-list expanded announce-to-customer permit _645XX:4[1357]000
bgp community-list expanded announce-to-peering permit _645XX:4[2367]000
bgp community-list expanded announce-to-upstream permit _645XX:4[4567]000
bgp community-list expanded my-communities permit _645XX:.*
bgp large-community-list expanded my-largecommunities permit _645XX:.*:.*
!
route-map upstream-in permit 50
 match community my-communities
 set comm-list my-communities delete
 on-match next
!
route-map upstream-in permit 60
 match large-community my-largecommunities
 set large-comm-list my-largecommunities delete
 on-match next
!
route-map upstream-in permit 100
 set community 64501:41000 additive
 set extcommunity rt 64501:21000
 set large-community 64501:2:1000 additive
 set local-preference 10
!
route-map peering-in permit 50
 match community my-communities
 set comm-list my-communities delete
 on-match next
!
route-map peering-in permit 60
 match large-community my-largecommunities
 set large-comm-list my-largecommunities delete
 on-match next
!
route-map peering-in permit 100
 set community 64501:41000 additive
 set extcommunity rt 64501:23000
 set large-community 64501:2:3000 additive
 set local-preference 1000
!
route-map customer-in permit 50
 match community my-communities
 set comm-list my-communities delete
 on-match next
!
route-map customer-in permit 60
 match large-community my-largecommunities
 set large-comm-list my-largecommunities delete
 on-match next
!
route-map customer-in permit 100
 set community 64501:47000 additive
 set extcommunity rt 64501:24000
 set large-community 64501:2:4000 additive
 set local-preference 10000
!
route-map customer-out permit 100
 match community announce-to-customer
!
route-map peering-out permit 100
 match community announce-to-peering
!
route-map upstream-out permit 100
 match community announce-to-upstream
!
route-map own-networks permit 100
 set community 64501:47000
!
line vty
!
end
\end{verbatim}

\section{Slides}
\iffalse
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge BGP Communities - informational}
\LARGE
Set communities that show from where you have received a prefix:

\begin{tabular}{|l|l|}
  \hline
  \rowcolor[gray]{.9}
  Received from & Community value \\
  \hline
  Upstream & 645xx:21000\\
  \hline
  Peer & 645xx:23000\\
  \hline
  Customer & 645xx:24000\\
  \hline
\end{tabular}

\vspace{1cm}

\begin{framed}
\begin{verbatim}
route-map upstream-in permit 100
  set community 645xx:21000 additive

route-map peering-in permit 100
  set community 645xx:23000 additive

route-map customer-in permit 100
  set community 645xx:24000 additive
\end{verbatim}
\end{framed}

Show commands:
\begin{itemize}
  \item show bgp ipv4 129.13.0.0/16
  \item show bgp ipv4 community 645\emph{xx}:23000
\end{itemize}
\end{samepage}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge BGP Extended Communities}
\LARGE
Set communities that show from where you have received a prefix:

\begin{tabular}{|l|l|}
  \hline
  \rowcolor[gray]{.9}
  Received from & Community value \\
  \hline
  Upstream & rt:645xx:21000\\
  \hline
  Peer & rt:645xx:23000\\
  \hline
  Customer & rt:645xx:24000\\
  \hline
\end{tabular}

\vspace{1cm}

\begin{framed}
\begin{verbatim}
  route-map upstream-in permit 100
    set extcommunity rt 645xx:21000

  route-map peering-in permit 100
    set extcommunity rt 645xx:23000

  route-map customer-in permit 100
    set extcommunity rt 645xx:24000
\end{verbatim}
\end{framed}

\begin{itemize}
  \item show bgp ipv4 129.13.0.0/16
  \item there is no "show bgp ipv4 extcommunity" command!
\end{itemize}
\end{samepage}
\fi
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge BGP Large Communities - Informational}
\LARGE

Set communities that show from where you have received a prefix:

\begin{tabular}{|l|l|}
  \hline
  \rowcolor[gray]{.9}
  Received from & Community value \\
  \hline
  Upstream & 645xx:2:1000\\
  \hline
  Peer & 645xx:2:3000\\
  \hline
  Customer & 645xx:2:4000\\
  \hline
\end{tabular}

\vspace{1cm}

\begin{framed}
\begin{verbatim}
route-map upstream-in permit 100
  set large-community 645xx:2:1000 additive

route-map peering-in permit 100
  set large-community 645xx:2:3000 additive

route-map customer-in permit 100
  set large-community 645xx:2:4000 additive
\end{verbatim}
\end{framed}

\begin{itemize}
  \item show bgp ipv4 129.13.0.0/16
  \item show bgp ipv4 large-community 645xx:2:4000
\end{itemize}

\end{samepage}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Large Community Lists - Standard}
\LARGE\textbf{Standard Large Community Lists}
\begin{itemize}
  \item Match on (complete) community numbers
  \item Exist for all three kinds of Communities
  \item Have permit/deny lines - first match terminates with result
\end{itemize}
\vspace{1cm}

Define three lists:

\begin{framed}
\Large
\begin{verbatim}
bgp large-community-list standard from-peering permit 645xx:2:3000

bgp large-community-list standard from-upstream permit 645xx:2:1000

bgp large-community-list standard from-customer permit 645xx:2:4000
\end{verbatim}
\end{framed}

Standard and extended community lists look very similar (no need to define them),
note that standard community lists have no special name as they are the oldest ones:
\begin{framed}
\begin{verbatim}
  bgp extcommunity-list standard ...

  bgp community-list standard ...
\end{verbatim}
\end{framed}

Show commands:
\begin{itemize}
  \item show bgp ipv4 large-community-list from-peering
  \item show bgp ipv4 large-community-list from-customer
\end{itemize}

\end{samepage}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Expanded Community Lists}
\LARGE
\begin{itemize}
  \item Use \emph{regular expressions} for matching
  \item Communities are seen as strings (characters) - not numbers
  \item See table:
\end{itemize}

\vspace{0.5cm}
\begin{tabular}{|l|l|}
  \hline
  \rowcolor[gray]{.9}
  Symbol/Character & Matches \\
  \hline
  0123456789 & number as written\\
  \hline
  \^{} & beginning of line\\
  \hline
  \$ & end of line\\
  \hline
  . & any character\\
  \hline
  \_ & any whitespace\\
  \hline
  $[123]$ & 1 or 2 or 3\\
  \hline
  * & zero or more occurrences of preceding pattern\\
  \hline
  + & one or more occurrences of preceding pattern\\
  \hline
  ? & zero or one occurrences of preceding pattern\\
  \hline
\end{tabular}

\vspace{1cm}

Define an expanded list:
\begin{framed}
\Large
\begin{verbatim}
bgp large-community-list expanded my-communities permit _645xx:.*
\end{verbatim}
\end{framed}

\begin{itemize}
  \item show bgp ipv4 large-community-list my-communities
\end{itemize}

\end{samepage}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge BGP Communities - Scrubbing \footnote{Remark: run script ex-04-02-wild-communities}}
\LARGE
\begin{itemize}
  \item Delete unwanted communities incoming
  \item We allow communities from customers
  \begin{itemize}
    \item But not from upstream or peers
    \item And also from customers only \emph{some} communities
  \end{itemize}
  \item All other (not our) communities should be kept
  \item Before you configure, do a \emph{"show bgp ipv4 44.5.0.0"} and note the attached standard communities
\end{itemize}

\begin{framed}
\Large
\begin{verbatim}
bgp large-community-list expanded my-communities permit _645xx:.*

route-map upstream-in permit 50
  match large-community my-communities
  set large-comm-list my-communities delete
  on-match next

route-map peering-in permit 50
  match large-community my-communities
  set large-comm-list my-communities delete
  on-match next

route-map customer-in permit 50
  match large-community my-communities
  set large-comm-list my-communities delete
  on-match next
\end{verbatim}
\end{framed}

\begin{itemize}
  \item now do \emph{"show bgp ipv4 44.5.0.0/16"} again and check the attached standard communities
  \item Feel free to scrub extended and standared communities as well.
\end{itemize}

\end{samepage}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge BGP Communities - Action!}
\LARGE
\begin{itemize}
  \item We \emph{set} communities when we receive prefixes
  \item These communities steer where prefixes are announced
\end{itemize}

For this we need an announcement policy:\\
\begin{tabular}{|l|l|}
  \hline
  \rowcolor[gray]{.9}
  \bf Received from & \bf Announced to \\
  \hline
  \bf Customers & Customers, Peers, Upstream\\
  \hline
  \bf Peers & Customers\\
  \hline
  \bf Upstream & Customers \\
  \hline
\end{tabular}

\vspace{1cm}
We use these communities:

\begin{tabular}{|l|l|c|}
  \hline
  \rowcolor[gray]{.9}
  \bf Announce to & \bf Large Communitiy & \bf Significant Digit \\
  \hline
  \bf Customers & 645xx:4:1 & 1\\
  \hline
  \bf Peers & 645xx:4:2 & 2\\
  \hline
  \bf Upstream & 645xx:4:4 & 4\\
  \hline
  \bf Peers + Upstream & 645xx:4:6 & 6 = 2 + 4\\
  \hline
  \bf Customers + Peers + Upstream & 645xx:4:7 & 7 = 1 + 2 + 4\\
  \hline
\end{tabular}

\vspace{1cm}

\begin{framed}
\LARGE
\begin{verbatim}
route-map customer-in permit 100
  set large-community 645xx:2:4000 645xx:4:7 additive

route-map upstream-in permit 100
  set large-community 645xx:2:1000 645xx:4:1 additive

route-map peering-in permit 100
  set large-community 645xx:2:3000 645xx:4:1 additive
\end{verbatim}
\end{framed}


\end{samepage}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge BGP Communities - Action!}
\LARGE
\begin{itemize}
  \item We need to change our outgoing filters
  \item For this we define community lists
  \item One list for each class of announcement
\end{itemize}

See this table for what we announce where:

\begin{tabular}{|l|l|l|}
  \hline
  \rowcolor[gray]{.9}
  \bf Announce to & \bf Large Communitiy & \bf Meaning \\
  \hline
  \bf Customers & 645xx:4:1 & Customers only\\
   & 645xx:4:3 & Customers+Peers\\
   & 645xx:4:5 & Customers+Upstream\\
   & 645xx:4:7 & All\\
  \hline
  \bf Peers & 645xx:4:2 & Peers only\\
   & 645xx:4:3 & Peers+Customers\\
   & 645xx:4:6 & Peers+Upstream\\
   & 645xx:4:7 & All\\
  \hline
  \bf Upstream & 645xx:4:4 & Upstream only\\
   & 645xx:4:5 & Upstream+Customers\\
   & 645xx:4:6 & Upstream+Peers\\
   & 645xx:4:7 & All\\
  \hline
\end{tabular}

Remember \emph{regular expressions}?

We can build nice and short expanded BGP community lists for matching:


\begin{framed}
\large
\begin{verbatim}
bgp large-community-list expanded to-customer permit _645xx:4:[1357]

bgp large-community-list expanded to-peering permit _645xx:4:[2367]

bgp large-community-list expanded to-upstream permit _645xx:4:[4567]
\end{verbatim}
\end{framed}

\begin{itemize}
  \item show bgp ipv4 large-community-list to-upstream
  \item show bgp ipv4 large-community-list to-peering
  \item show bgp ipv4 large-community-list to-customer
\end{itemize}

\end{samepage}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge BGP Communities - Action!}
\LARGE
\begin{itemize}
  \item We now use these community list in route-map \emph{match} statements
  \item We replace current route-maps for outgoing announcements
\end{itemize}

\begin{framed}
\begin{verbatim}
  route-map customer-out permit 100
    match large-community to-customer

  route-map peering-out permit 100
    match large-community to-peering
    no match  as-path my-as

  route-map upstream-out permit 100
    match large-community to-upstream
    no match as-path my-as
\end{verbatim}
\end{framed}

Show commands:
\begin{itemize}
  \item show bgp ipv4 neighbors 10.230.xx.1 advertised-routes
  \item show bgp ipv4 neighbors 80.81.192.1yy advertised-routes
  \item show bgp ipv4 neighbors 10.200.xx.1 advertised-routes
\end{itemize}

\end{samepage}
%----------------------------------------------------------------
\clearpage
\begin{samepage}
\subsection*{\Huge Advertising our own network}
\LARGE
\begin{itemize}
  \item We have a static route for 10.xx.0.0/16
  \item We inject it into BGP using a network statement
  \item But it is not announced anywhere
  \begin{itemize}
    \item Because no communities are attached to it
  \end{itemize}
  \item We can re-use the customer-in route-map
  \begin{itemize}
    \item Or define a separate one (to avoid side effects)
  \end{itemize}
\end{itemize}

\begin{framed}
\begin{verbatim}
  route-map own-networks permit 100
    set large-community 645xx:4:7

  router bgp 645xx
    address-family ipv4 unicast
      network 10.xx.0.0/16 route-map own-networks
\end{verbatim}
\end{framed}

Show commands:
\begin{itemize}
  \item show bgp ipv4 10.xx.0.0/16
  \item show bgp ipv4 neighbors 10.230.xx.1 advertised-routes
  \item show bgp ipv4 neighbors 80.81.192.1yy advertised-routes
  \item show bgp ipv4 neighbors 10.200.xx.1 advertised-routes
\end{itemize}


\end{samepage}
%----------------------------------------------------------------



\end{document}
